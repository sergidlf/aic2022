package testplayer;

import aic2022.user.*;

public class AssassinPathfinder {

    UnitController uc;
    final int INF = 1000000000;
    public final Constants constants = new Constants();
    Helper helper;
    boolean rotateRight = true; //if I should rotate right or left
    boolean rotate = false;
    Location lastObstacleFound = null; //latest obstacle I've found in my way
    int minDistToEnemy = INF; //minimum distance I've been to the enemy while going around an obstacle
    Location prevTarget = null; //previous target
    Location myLoc;
    Location enemyBase = null;
    Team myTeam;
    UnitInfo[] enemies;
    UnitInfo[] enemies2;
    Direction microDir;
    Direction[] myDirs;
    float baseRange;
    boolean isEnemies;
    UnitType myType;
    Location closestChest = null;

    AssassinPathfinder(UnitController uc){
        this.myDirs = Direction.values();
        this.uc = uc;
        this.myTeam = uc.getTeam();
        this.baseRange = UnitType.BASE.getStat(UnitStat.ATTACK_RANGE);
        this.myType = uc.getType();
        this.helper = new Helper(uc);
    }

    void getClosestChest() {
        ChestInfo[] chests = uc.senseChests();
        if (chests.length != 0) closestChest = chests[0].getLocation();
        else closestChest = null;
    }

    void setEnemyBase(Location target) {
        enemyBase = target;
    }

    Boolean getNextLocationTarget(Location target, boolean reckless, boolean reachTarget){
        if (enemyBase == null) {
            int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
            if (position != 0) setEnemyBase(helper.intToRealLocation(position));
        }

        if (!uc.canMove()) return false;
        if (target == null) return false;
        getClosestChest();
        isEnemies = false;

        //different target? ==> previous data does not help!
        if (prevTarget == null || !target.isEqual(prevTarget)) resetPathfinding();

        //If I'm at a minimum distance to the target, I'm free!
        myLoc = uc.getLocation();
        int d = myLoc.distanceSquared(target);
        if (d <= minDistToEnemy) resetPathfinding();

        //Update data
        prevTarget = target;
        minDistToEnemy = Math.min(d, minDistToEnemy);

        //If there's an obstacle I try to go around it [until I'm free] instead of going to the target directly
        Direction dir = myLoc.directionTo(target);
        if (lastObstacleFound != null) dir = myLoc.directionTo(lastObstacleFound);

        //This should not happen for a single unit, but whatever
        if (uc.canMove(dir)) resetPathfinding();

        //I rotate clockwise or counterclockwise (depends on 'rotateRight'). If I try to go out of the map I change the orientation
        //Note that we have to try at most 16 times since we can switch orientation in the middle of the loop. (It can be done more efficiently)
        if (!reckless) doMicro();
        else microDir = null;

        if (microDir != null) {
            if (microDir != Direction.ZERO) uc.move(microDir);
            return true;
        }
        else if (!isEnemies) {
            if (!reachTarget) {
                int currentDistance = myLoc.distanceSquared(target);
                if (currentDistance <= myType.getStat(UnitStat.ATTACK_RANGE)) return false;
                else if (currentDistance < myType.getStat(UnitStat.MIN_ATTACK_RANGE)) {
                    if (uc.canMove(myLoc.directionTo(target).opposite())) {
                        uc.move(myLoc.directionTo(target).opposite());
                        return true;
                    }
                }
            }
            for (int i = 0; i < 16; ++i) {
                for (int j = 0; j < myDirs.length; j++) {
                    if (myDirs[j] == dir) {
                        Location loc = myLoc.add(dir);
                        if (uc.canMove(dir) && ((!isEnemies && (enemyBase == null || (loc.distanceSquared(enemyBase) > baseRange) || (uc.canSenseLocation(enemyBase) && uc.isObstructed(loc, enemyBase)))) || reckless)) {
                            uc.move(dir);
                            return true;
                        }
                        break;
                    }
                }
                if (!rotate && myLoc.add(dir.rotateLeft()).distanceSquared(target) > myLoc.add(dir.rotateRight()).distanceSquared(target)) {
                    rotateRight = true;
                    rotate = true;
                }
                Location newLoc = myLoc.add(dir);
                if (uc.isOutOfMap(newLoc)) rotateRight = !rotateRight;
                    //If I could not go in that direction and it was not outside of the map, then this is the latest obstacle found
                else lastObstacleFound = myLoc.add(dir);
                if (rotateRight) dir = dir.rotateRight();
                else dir = dir.rotateLeft();
            }

            for (int j = 0; j < myDirs.length; j++) {
                if (myDirs[j] == dir) {
                    Location loc = myLoc.add(dir);
                    if (uc.canMove(dir) && ((!isEnemies && (enemyBase == null || (loc.distanceSquared(enemyBase) > baseRange) || (uc.canSenseLocation(enemyBase) && uc.isObstructed(loc, enemyBase)))) || reckless)) {
                        uc.move(dir);
                        return true;
                    }
                    break;
                }
            }
        }

        return false;
    }

    void resetPathfinding(){
        lastObstacleFound = null;
        minDistToEnemy = INF;
    }

    public void doMicro() {
        MicroInfo[] microInfo = new MicroInfo[9];
        enemies = uc.senseUnits(myTeam.getOpponent());
        enemies2 = uc.senseUnits(Team.NEUTRAL);
        for (int i = 0; i < 9; i++) {
            if (!uc.canMove(myDirs[i])) continue;
            Location target = myLoc.add(myDirs[i]);
            int chestDistance = 100000;
            if (closestChest != null) chestDistance = target.distanceSquared(closestChest);
            microInfo[i] = new MicroInfo(myLoc.add(myDirs[i]));

            if (enemyBase != null && target.distanceSquared(enemyBase) <= baseRange) {
                if (uc.canSenseLocation(enemyBase) && !uc.isObstructed(target, enemyBase) || !uc.canSenseLocation(enemyBase)) microInfo[i].damage += 2000;
            }

            for (int j = 0; j < Math.min(enemies.length, 7); j++) {
                Location enemyLoc = enemies[j].getLocation();
                if (uc.canSenseLocation(enemyLoc) && uc.canSenseLocation(target) && uc.isObstructed(enemyLoc, target)) continue;
                if (!uc.canSenseLocation(target)) continue;
                isEnemies = true;
                UnitInfo enemy = enemies[j];
                int distance = microInfo[i].loc.distanceSquared(enemy.getLocation());
                microInfo[i].updateSafe(distance, enemy, chestDistance);
            }

            for (int j = 0; j < Math.min(enemies2.length, 7); j++) {
                Location enemyLoc = enemies2[j].getLocation();
                if (uc.canSenseLocation(enemyLoc) && uc.canSenseLocation(target) && uc.isObstructed(enemyLoc, target)) continue;
                if (!uc.canSenseLocation(target)) continue;
                isEnemies = true;
                UnitInfo enemy = enemies2[j];
                int distance = microInfo[i].loc.distanceSquared(enemy.getLocation());
                microInfo[i].updateSafe(distance, enemy, chestDistance);
            }
        }

        int bestIndex = 8;

        for (int i = 8; i >= 0; i--) {
            if (!uc.canMove(myDirs[i])) continue;
            if (!microInfo[bestIndex].isBetter(microInfo[i])) bestIndex = i;
        }

        if (isEnemies) {
            if (microInfo[bestIndex].minDistToEnemy < 9 && uc.getInfo().getCurrentAbilityICooldown() == 0) {
                UnitInfo[] allEnemies = uc.senseUnits((int)uc.getInfo().getStat(UnitStat.VISION_RANGE), myTeam, true);
                Location[] locations = uc.getVisibleLocations((int)uc.getInfo().getStat(UnitStat.ABILITY_1_RANGE));

                for (int i = locations.length - 1; i > 8; i--) {
                    if (!uc.canUseFirstAbility(locations[i])) continue;
                    boolean canFlee = true;

                    for (UnitInfo enemy : allEnemies) {
                        Location enemyLoc = enemy.getLocation();
                        int distance = locations[i].distanceSquared(enemyLoc);
                        boolean canMove = enemy.getCurrentMovementCooldown() < 2;

                        if (canMove && distance <= 8) canFlee = false;
                        else if (!canMove && distance <= 2) canFlee = false;
                    }
                    if (canFlee) uc.useFirstAbility(locations[i]);
                    microDir = null;
                    return;
                }
            }
            microDir = myDirs[bestIndex];
        } else microDir = null;
    }

    class MicroInfo {
        int damage;
        int softdamage;
        int minDistToEnemy;
        int minDistToRanger;
        int minDistToChest;
        Location loc;
        boolean isShrine;
        boolean isStraight;

        public MicroInfo(Location loc) {
            this.loc = loc;
            this.isShrine = uc.senseTileTypeAtLocation(loc) == TileType.SHRINE;
            this.isStraight = (loc.x == myLoc.x || loc.y == myLoc.y);
            damage = 0;
            minDistToRanger = 100000;
            minDistToEnemy = 100000;
            minDistToChest = 100000;
        }

        void updateSafe(int distance, UnitInfo enemy, int chestDistance) {
            UnitType enemyType = enemy.getType();

            if (enemyType == UnitType.BARBARIAN) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                }
            } else if (enemyType == UnitType.RANGER) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE) && distance >= enemy.getStat(UnitStat.MIN_ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                }
            } else if (enemyType == UnitType.KNIGHT) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                }
            } else if (enemyType == UnitType.BASE) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                }
            } else if (enemyType == UnitType.EXPLORER) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                }
            } else if (enemyType == UnitType.MAGE) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                    softdamage += enemy.getStat(UnitStat.ATTACK);
                }
            } else if (enemyType == UnitType.ASSASSIN) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                }
            }

            if (distance < minDistToEnemy && enemyType != UnitType.RANGER) minDistToEnemy = distance;
            if (distance < minDistToRanger && enemyType == UnitType.RANGER) minDistToRanger = distance;
            minDistToChest = chestDistance;
        }

        boolean isBetter(MicroInfo m) {
            if (uc.readOnSharedArray(constants.DESTROY_BASE) == 0 && enemyBase != null && uc.canSenseLocation(enemyBase) && enemies.length == 1) return minDistToEnemy >= m.minDistToEnemy;

            if ((!isShrine && !m.isShrine) || (isShrine && m.isShrine)) {
                if (uc.getInfo().getCurrentAttackCooldown() == uc.getInfo().getStat(UnitStat.ATTACK_COOLDOWN)) {
                    if (damage == m.damage) {
                        if (isStraight && m.isStraight) return minDistToChest <= m.minDistToChest;
                        if (isStraight) return true;
                        return false;
                    }
                    return damage <= m.damage;
                }

                if (minDistToEnemy == 100000) {
                    return minDistToRanger <= m.minDistToRanger;
                }

                if (minDistToEnemy < 9 && m.minDistToEnemy < 9) {
                    if (minDistToEnemy == m.minDistToEnemy) {
                        if (isStraight && m.isStraight) return minDistToChest <= m.minDistToChest;
                        if (isStraight) return true;
                        return false;
                    }
                    return minDistToEnemy >= m.minDistToEnemy;
                }
                if (minDistToEnemy < 9) return false;
                if (m.minDistToEnemy < 9) return true;

                if (minDistToChest != 100000) {
                    if (isStraight && m.isStraight) return minDistToChest <= m.minDistToChest;
                    if (isStraight) return true;
                    return false;
                }

                if (minDistToEnemy == m.minDistToEnemy) {
                    if (isStraight) return true;
                    return false;
                }
                return minDistToEnemy <= m.minDistToEnemy;
            }
            if (isShrine) return false;
            return true;
        }
    }
}