package testplayer;

import aic2022.user.*;

public class Attack {
    UnitController uc;
    private final Constants constants = new Constants();
    private final Helper helper;
    UnitType myType;
    Team myTeam;
    Location enemyBase = null;

    public Attack(UnitController uc) {
        this.uc = uc;
        this.helper = new Helper(uc);
        this.myType = uc.getType();
        this.myTeam = uc.getTeam();
    }

    boolean couldAttackLocation(Location loc) {
        Location currentLoc = uc.getLocation();
        Location lastLoc = currentLoc;
        Location lastWalkableTile = currentLoc;
        while (currentLoc.distanceSquared(loc) > 0) {
            lastLoc = lastLoc.add(lastLoc.directionTo(loc));
            TileType tt = uc.senseTileTypeAtLocation(lastLoc);
            if (tt == TileType.MOUNTAIN) return false;
            if (tt == TileType.WATER || lastLoc.isEqual(loc)) {
                break;
            }
            lastWalkableTile = lastLoc;
        }
        if (lastWalkableTile.distanceSquared(loc) > uc.getType().getStat(UnitStat.ATTACK_RANGE)) {
            return false;
        }
        return true;
    }

    boolean couldAttackLocationBase(Location loc) {
        Location currentLoc = uc.getLocation();
        Location lastLoc = currentLoc;
        Location lastWalkableTile = currentLoc;
        while (currentLoc.distanceSquared(loc) > 0) {
            lastLoc = lastLoc.add(lastLoc.directionTo(loc));
            TileType tt = uc.senseTileTypeAtLocation(lastLoc);
            if (tt == TileType.MOUNTAIN) return false;
            if (tt == TileType.WATER || lastLoc.isEqual(loc)) {
                break;
            }
            lastWalkableTile = lastLoc;
        }
        if (lastWalkableTile.distanceSquared(loc) > UnitType.RANGER.getStat(UnitStat.ATTACK_RANGE)) {
            return false;
        }
        return true;
    }

    public boolean tryHeal(UnitInfo[] allies) {
        if (!uc.canAttack()) return false;

        int highestHealth = 0;
        int lowestHealth = 100000;
        Location highestTarget = null;
        Location lowestTarget = null;

        for (UnitInfo ally : allies) {
            Location loc = ally.getLocation();
            int health = ally.getHealth();
            if (health == ally.getStat(UnitStat.MAX_HEALTH)) continue;
            if (uc.canAttack(loc)) {
                if (health > highestHealth) {
                    if (ally.getType() != UnitType.BASE) {
                        highestHealth = health;
                        highestTarget = ally.getLocation();
                    }
                }
                if (health < lowestHealth) {
                    lowestHealth = health;
                    lowestTarget = ally.getLocation();
                }
            }
        }

        if (lowestHealth < 100 || highestTarget == null) {
            uc.attack(lowestTarget);
            return true;
        } else if (highestTarget != null) {
            uc.attack(highestTarget);
        }
        return false;
    }

    public boolean tryAttackShrine(ShrineInfo[] shrines) {
        ShrineInfo alliedShrine = null;
        ShrineInfo neutralShrine = null;
        ShrineInfo enemyShrine = null;

        for (ShrineInfo shrine : shrines) {
            Location shrineLoc = shrine.getLocation();
            if(!uc.getLocation().isEqual(shrineLoc)) {
                if (uc.canAttack(shrineLoc)) {
                    Team shrineOwner = shrine.getOwner();
                    if (shrineOwner == myTeam && !shrine.hasFullInfluence()) alliedShrine = shrine;
                    if (shrineOwner == Team.NEUTRAL) neutralShrine = shrine;
                    if (shrineOwner == myTeam.getOpponent()) enemyShrine = shrine;
                }
            }
        }

        if (enemyShrine != null) {
            uc.attack(enemyShrine.getLocation());
            return true;
        } else if (neutralShrine != null) {
            uc.attack(neutralShrine.getLocation());
            return true;
        } else if (alliedShrine != null) {
            uc.attack(alliedShrine.getLocation());
            return true;
        }

        return false;
    }

    public boolean tryAttackShrineAssassin(ShrineInfo[] shrines, UnitInfo[] enemies, UnitInfo[] neutrals) {
        if (!uc.canAttack()) return false;

        Location myLoc = uc.getLocation();
        int damage = 0;

        for (UnitInfo enemy : enemies) {
            Location enemyLoc = enemy.getLocation();
            UnitType enemyType = enemy.getType();
            if (enemy.getCurrentAttackCooldown() >= 2) continue;

            int distance = enemyLoc.distanceSquared(myLoc);

            if (distance < 19 && enemyType == UnitType.BARBARIAN) return false;
            if (distance < 14 && enemyType == UnitType.KNIGHT) return false;

            if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE) && distance >= enemy.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += enemy.getStat(UnitStat.ATTACK);
        }

        for (UnitInfo neutral : neutrals) {
            Location enemyLoc = neutral.getLocation();
            UnitType enemyType = neutral.getType();
            if (neutral.getCurrentAttackCooldown() >= 2) continue;

            int distance = enemyLoc.distanceSquared(myLoc);

            if (distance < 19 && enemyType == UnitType.BARBARIAN) return false;
            if (distance < 14 && enemyType == UnitType.KNIGHT) return false;

            if (distance <= neutral.getStat(UnitStat.ATTACK_RANGE) && distance >= neutral.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += neutral.getStat(UnitStat.ATTACK);
        }

        if (damage <= 0) {
            for (ShrineInfo shrine : shrines) {
                if (shrine.getOwner() == myTeam) continue;

                Location shrineLoc = shrine.getLocation();
                if(!uc.getLocation().isEqual(shrineLoc)) {
                    if (uc.canAttack(shrineLoc)) {
                        uc.attack(shrineLoc);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean assassinAttackBase(Location enemyBase) {
        if (enemyBase == null) return false;
        if (!uc.canSenseLocation(enemyBase)) return false;

        UnitInfo target = uc.senseUnitAtLocation(enemyBase);
        Direction[] dirs = Direction.values();
        double bestDamage = 0;
        Location bestLoc = null;

        for (Direction dir : dirs) {
            Location adjacentLoc = enemyBase.add(dir);
            if (uc.canUseFirstAbility(adjacentLoc)) {
                double currentDamage = calculateDamage(target, adjacentLoc);
                if (currentDamage > bestDamage) {
                    currentDamage = bestDamage;
                    bestLoc = adjacentLoc;
                }
            }
        }

        if (bestLoc != null) {
            uc.useFirstAbility(bestLoc);
            uc.attack(enemyBase);
            return true;
        }

        return false;
    }

    public boolean assassinAttack(UnitInfo[] enemies, UnitInfo[] neutrals) {
        if (uc.getInfo().getCurrentAbilityICooldown() == 0 && uc.getInfo().getCurrentAttackCooldown() == 0 && uc.canMove()) {
            Direction[] dirs = Direction.values();
            Location shadowTargetKill = null;
            Location enemyTargetKill = null;
            Location shadowTargetNoKill = null;
            Location enemyTargetNoKill = null;
            int lowestDamage = 100000;
            double bestDamage = 0;

            for (UnitInfo unit : enemies) {
                if (uc.getEnergyLeft() < 15000) continue;
                Location target = unit.getLocation();
                int targetHealth = unit.getHealth();
                for (Direction dir : dirs) {
                    Location adjacentLoc = target.add(dir);
                    if (uc.canUseFirstAbility(adjacentLoc)) {
                        double currentDamage = calculateDamage(unit, adjacentLoc);
                        if (currentDamage >= targetHealth) {
                            int damage = 0;
                            for (UnitInfo enemy : enemies) {
                                Location enemyLoc = enemy.getLocation();
                                if (enemyLoc.equals(target)) continue;
                                if (enemy.getCurrentAttackCooldown() >= 2) continue;
                                if (enemy.getType() == UnitType.CLERIC) continue;

                                int distance = enemyLoc.distanceSquared(adjacentLoc);

                                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE) && distance >= enemy.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += enemy.getStat(UnitStat.ATTACK);
                            }

                            for (UnitInfo neutral : neutrals) {
                                Location enemyLoc = neutral.getLocation();
                                if (enemyLoc.equals(target)) continue;
                                if (neutral.getCurrentAttackCooldown() >= 2) continue;
                                if (neutral.getType() == UnitType.CLERIC) continue;

                                int distance = enemyLoc.distanceSquared(adjacentLoc);

                                if (distance <= neutral.getStat(UnitStat.ATTACK_RANGE) && distance >= neutral.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += neutral.getStat(UnitStat.ATTACK);
                            }

                            if (enemyBase != null) {
                                if (enemyBase.distanceSquared(adjacentLoc) <= 32) damage += 70;
                            }

                            if (damage < 23 && damage < uc.getInfo().getHealth() && damage < lowestDamage) {
                                shadowTargetKill = adjacentLoc;
                                enemyTargetKill = target;
                                lowestDamage = damage;
                            }
                        } else {
                            int damage = 0;
                            for (UnitInfo enemy : enemies) {
                                if (enemy.getCurrentAttackCooldown() >= 2) continue;
                                if (enemy.getType() == UnitType.CLERIC) continue;

                                Location enemyLoc = enemy.getLocation();
                                int distance = enemyLoc.distanceSquared(adjacentLoc);

                                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE) && distance >= enemy.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += enemy.getStat(UnitStat.ATTACK);
                            }

                            for (UnitInfo neutral : neutrals) {
                                Location enemyLoc = neutral.getLocation();
                                if (neutral.getCurrentAttackCooldown() >= 2) continue;
                                if (neutral.getType() == UnitType.CLERIC) continue;

                                int distance = enemyLoc.distanceSquared(adjacentLoc);

                                if (distance <= neutral.getStat(UnitStat.ATTACK_RANGE) && distance >= neutral.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += neutral.getStat(UnitStat.ATTACK);
                            }

                            if (enemyBase != null) {
                                if (enemyBase.distanceSquared(adjacentLoc) <= UnitType.BASE.getStat(UnitStat.VISION_RANGE)) damage += 70;
                            }

                            if (currentDamage > bestDamage && damage <= 0) {
                                bestDamage = currentDamage;
                                shadowTargetNoKill = adjacentLoc;
                                enemyTargetNoKill = target;
                            }
                        }
                    }
                }
            }

            for (UnitInfo unit : neutrals) {
                if (uc.getEnergyLeft() < 15000) continue;
                Location target = unit.getLocation();
                int targetHealth = unit.getHealth();
                for (Direction dir : dirs) {
                    Location adjacentLoc = target.add(dir);
                    if (uc.canUseFirstAbility(adjacentLoc)) {
                        double currentDamage = calculateDamage(unit, adjacentLoc);
                        if (currentDamage >= targetHealth) {
                            int damage = 0;
                            for (UnitInfo enemy : enemies) {
                                Location enemyLoc = enemy.getLocation();
                                if (enemyLoc.equals(target)) continue;
                                if (enemy.getCurrentAttackCooldown() >= 2) continue;

                                int distance = enemyLoc.distanceSquared(adjacentLoc);

                                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE) && distance >= enemy.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += enemy.getStat(UnitStat.ATTACK);
                            }

                            for (UnitInfo neutral : neutrals) {
                                Location enemyLoc = neutral.getLocation();
                                if (enemyLoc.equals(target)) continue;
                                if (neutral.getCurrentAttackCooldown() >= 2) continue;

                                int distance = enemyLoc.distanceSquared(adjacentLoc);

                                if (distance <= neutral.getStat(UnitStat.ATTACK_RANGE) && distance >= neutral.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += neutral.getStat(UnitStat.ATTACK);
                            }

                            if (damage < 23 && damage < uc.getInfo().getHealth() && damage < lowestDamage) {
                                shadowTargetKill = adjacentLoc;
                                enemyTargetKill = target;
                                lowestDamage = damage;
                            }
                        } else {
                            int damage = 0;
                            for (UnitInfo enemy : enemies) {
                                Location enemyLoc = enemy.getLocation();
                                if (enemy.getCurrentAttackCooldown() >= 2) continue;

                                int distance = enemyLoc.distanceSquared(adjacentLoc);

                                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE) && distance >= enemy.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += enemy.getStat(UnitStat.ATTACK);
                            }

                            for (UnitInfo neutral : neutrals) {
                                Location enemyLoc = neutral.getLocation();
                                if (neutral.getCurrentAttackCooldown() >= 2) continue;

                                int distance = enemyLoc.distanceSquared(adjacentLoc);

                                if (distance <= neutral.getStat(UnitStat.ATTACK_RANGE) && distance >= neutral.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += neutral.getStat(UnitStat.ATTACK);
                            }

                            if (currentDamage > bestDamage && damage <= 0) {
                                bestDamage = currentDamage;
                                shadowTargetNoKill = adjacentLoc;
                                enemyTargetNoKill = target;
                            }
                        }
                    }
                }
            }

            if (shadowTargetKill != null) {
                Direction dir = uc.getLocation().directionTo(shadowTargetKill);
                if (uc.canMove(dir) && uc.getLocation().distanceSquared(shadowTargetKill) <= 2) uc.move(dir);
                else uc.useFirstAbility(shadowTargetKill);
                uc.attack(enemyTargetKill);
                return true;
            } else if (shadowTargetNoKill != null) {
                Direction dir = uc.getLocation().directionTo(shadowTargetNoKill);
                if (uc.canMove(dir) && uc.getLocation().distanceSquared(shadowTargetNoKill) <= 2) uc.move(dir);
                else uc.useFirstAbility(shadowTargetNoKill);
                uc.attack(enemyTargetNoKill);
                return true;
            }

        }
        return false;
    }

    double calculateDamage(UnitInfo unit, Location loc) {
        float myAttack = uc.getInfo().getStat(UnitStat.ATTACK);

        Location target = unit.getLocation();

        boolean isForest = uc.senseTileTypeAtLocation(target) == TileType.FOREST;
        boolean isRangerLVL4 = unit.getType() == UnitType.RANGER && unit.getLevel() == 4;
        boolean isAssassinLVL4 = myType == UnitType.ASSASSIN && uc.getInfo().getLevel() == 4;
        double multiplier1 = 0.75;
        double multiplier2 = 1.25;
        if (isAssassinLVL4) {
            multiplier1 = 0.75;
            multiplier2 = 2.25;
        }

        float armor = unit.getType().getStat(UnitStat.DEFENSE);
        float attackAfterDefense = myAttack - armor;
        Direction orientation = unit.getOrientation();
        double forestReduction = 1;
        if (isForest) {
            if (isRangerLVL4) forestReduction = 0.5;
            else forestReduction = 0.8;
        }

        int dx = loc.x - target.x;
        int dy = loc.y - target.y;

        int ux = 0;
        int uy = 0;

        if (orientation == Direction.NORTH) {
            ux = 0;
            uy = 1;
        } else if (orientation == Direction.SOUTH) {
            ux = 0;
            uy = -1;
        } else if (orientation == Direction.EAST) {
            ux = 1;
            uy = 0;
        } else if (orientation == Direction.WEST) {
            ux = -1;
            uy = 0;
        } else if (orientation == Direction.NORTHEAST) {
            ux = 1;
            uy = 1;
        } else if (orientation == Direction.NORTHWEST) {
            ux = -1;
            uy = 1;
        } else if (orientation == Direction.SOUTHEAST) {
            ux = 1;
            uy = -1;
        } else if (orientation == Direction.SOUTHWEST) {
            ux = -1;
            uy = -1;
        }

        int num = (ux*dx + uy*dy);
        double den = (Math.sqrt(Math.pow(ux, 2) + Math.pow(uy, 2)) * (Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2))) );
        double angle = (Math.acos(num / den) * 180 / Math.PI);

        double damage = Math.ceil((multiplier1 + (angle / 180) * multiplier2) * attackAfterDefense * forestReduction);
        if (damage < 4) damage = 4;

        return damage;
    }

    public boolean genericTryAttack(UnitInfo[] units)  {
        if (!uc.canAttack()) return false;

        Location myLoc = uc.getLocation();

        UnitInfo bestTarget = null;
        int bestTargetHealth = 10000;
        Location bestLoc = null;
        UnitInfo killableTarget = null;
        int killableTargetHealth = 0;
        Location killableLoc = null;
        UnitInfo killableEnemy = null;
        int killableEnemyHealth = 0;
        Location killableEnemyLoc = null;
        UnitInfo bestEnemy = null;
        int bestTargetEnemy = 10000;
        Location bestEnemyLoc = null;

        for (UnitInfo unit : units) {
            Location target = unit.getLocation();

            if (enemyBase == null && unit.getType() == UnitType.BASE) {
                enemyBase = target;
                uc.writeOnSharedArray(constants.ENEMY_BASE_POSITION, helper.realLocationToInt(target.x, target.y));
            }

            if (uc.canAttack(target)) {
                int health = unit.getHealth();

                if (bestTargetHealth > health) {
                    bestTarget = unit;
                    bestTargetHealth = health;
                    bestLoc = target;
                }
                if (bestTargetEnemy < health) {
                    bestEnemy = unit;
                    bestTargetEnemy = health;
                    bestEnemyLoc = target;
                }
                if (calculateDamage(unit, myLoc) >= health) {
                    if (killableTargetHealth < health) {
                        killableTarget = unit;
                        killableTargetHealth = health;
                        killableLoc = target;
                    }
                    if (killableEnemyHealth < health) {
                        killableEnemy = unit;
                        killableEnemyHealth = health;
                        killableEnemyLoc = target;
                    }
                }
            }
        }

        if (killableEnemy != null) {
            uc.attack(killableEnemyLoc);
            return true;
        } else if (bestEnemy != null) {
            uc.attack(bestEnemyLoc);
            return true;
        } else if (killableTarget != null) {
            uc.attack(killableLoc);
            return true;
        } else if (bestTarget != null) {
            uc.attack(bestLoc);
            return true;
        }

        return false;
    }
}
