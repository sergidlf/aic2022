package testplayer;

import aic2022.user.*;

public class Base extends MyUnit {

    int round = 0;
    int roundSpawn = 0;
    Location enemyBase = null;
    Boolean isBaseClose = false;
    UnitType target;
    Direction[] safeSpawn = new Direction[8];
    boolean spawnedExplorer = false;
    int assassinCounter = 0;
    boolean spawnedCleric = false;
    boolean woundedAlly = false;
    int spawnCounter = 0;
    Location lastEnemy = null;

    Base(UnitController uc){
        super(uc);

        this.uc = uc;
    }

    void playRound(){
        round = uc.getRound();
        if (round == 0) init();
        if (round % 400 == 0) manager.resetDungeons();
        int enemyPos = uc.readOnSharedArray(constants.LAST_ENEMY_LOCATION);
        if (enemyPos != 0) lastEnemy = helper.intToRealLocation(enemyPos);
        if (uc.readOnSharedArray(constants.LAST_ROUND_ENEMY) + 1 < round) uc.writeOnSharedArray(constants.LAST_ENEMY_LOCATION, 0);

        if (uc.readOnSharedArray(constants.EXPLORER_ALIVE) == 0) spawnedExplorer = false;
        if (uc.readOnSharedArray(constants.CLERIC_ALIVE) == 0) spawnedCleric = false;
        assassinCounter = uc.readOnSharedArray(constants.ASSASSIN_ALIVE);

        if (enemyBase == null) {
            int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
            if (position != 0) enemyBase = helper.intToRealLocation(position);
        }

        UnitInfo[] enemies = uc.senseUnits(opponent);
        UnitInfo[] neutrals = uc.senseUnits(Team.NEUTRAL);
        UnitInfo[] allies = uc.senseUnits(myTeam);

        int dangerLevel = senseDanger(enemies, neutrals);
        checkWoundedAllies(allies);
        checkEnemyLevels(enemies);

        attack.genericTryAttack(enemies);
        attack.genericTryAttack(neutrals);
        attack.tryAttackShrine(uc.senseShrines());

        trySpawn(dangerLevel);
        resetCounters();
    }

    void checkWoundedAllies(UnitInfo[] allies) {
        for (UnitInfo ally : allies) {
            if (ally.getHealth() < ally.getStat(UnitStat.MAX_HEALTH)) {
                woundedAlly = true;
                break;
            }
        }
    }

    void resetCounters() {
        uc.writeOnSharedArray(constants.ASSASSIN_ALIVE, 0);
        uc.writeOnSharedArray(constants.EXPLORER_ALIVE, 0);
        uc.writeOnSharedArray(constants.CLERIC_ALIVE, 0);
        uc.writeOnSharedArray(constants.UNIT_COUNTER, 0);
    }

    void init() {
        allyBase = uc.getLocation();
        uc.writeOnSharedArray(constants.BASE_POSITION, helper.realLocationToInt(allyBase.x, allyBase.y));
        setMapEdges();
        senseBase();
        senseWater();
    }

    void setMapEdges() {
        uc.writeOnSharedArray(constants.MAP_EDGE_X1, -1);
        uc.writeOnSharedArray(constants.MAP_EDGE_X2, 1100);
        uc.writeOnSharedArray(constants.MAP_EDGE_Y1, -1);
        uc.writeOnSharedArray(constants.MAP_EDGE_Y2, 1100);
    }

    private void trySpawn(int dangerLevel){
        if (round == 0) target = UnitType.EXPLORER;
        if (uc.readOnSharedArray(constants.WATER_SHRINES) == 1) target = UnitType.RANGER;
        if (dangerLevel == constants.BASE_ATTACKED) target = UnitType.RANGER;
        if (((uc.getReputation() >= 180 && assassinCounter < 1) || (uc.getReputation() >= 240 && assassinCounter < 2)) && round < 1200) {
            target = UnitType.ASSASSIN;
        }
        if (target == UnitType.ASSASSIN && ((uc.getReputation() < 180 && assassinCounter == 0) || (uc.getReputation() < 240 && assassinCounter == 1) || round > 1200)) {
            target = null;
        }
        if (dangerLevel == constants.BASE_HEAVILY_ATTACKED) target = UnitType.KNIGHT;
        if (!spawnedCleric && dangerLevel == constants.BASE_SAFE && round < 1200 &&
                uc.getInfo().getHealth() < uc.getInfo().getStat(UnitStat.MAX_HEALTH) / 3) {
            target = UnitType.CLERIC;
        }

        if (target == null) {
            if (spawnCounter == 0 || spawnCounter == 2 || (spawnCounter == 3 && uc.readOnSharedArray(constants.HAS_WATER) == 1) || (spawnCounter == 1 && uc.readOnSharedArray(constants.HAS_A_LOT_OF_WATER) == 1)) {
                target = UnitType.RANGER;
            }
            else target = UnitType.KNIGHT;
        }

        if (spawnSafe(target)) {
            if (target == UnitType.CLERIC) spawnedCleric = true;
            if (target == UnitType.EXPLORER) spawnedExplorer = true;
            if (target == UnitType.RANGER) uc.writeOnSharedArray(constants.WATER_SHRINES, 0);
            spawnCounter++;
            if (spawnCounter == 4) spawnCounter = 0;
            target = null;
        }
    }

    private boolean spawnSafe(UnitType t) {
        Direction[] myDirs = new Direction[8];
        int index = 0;
        Location myLoc = uc.getLocation();

        for (Direction dir: safeSpawn) {
            if (uc.canSpawn(t, dir)) {
                myDirs[index] = dir;
                index++;
            }
        }

        if (myDirs[0] == null) return false;

        UnitInfo[] enemies = uc.senseUnits((int)UnitType.BASE.getStat(UnitStat.VISION_RANGE), myTeam, true);

        if (enemies.length == 0) {
            if (lastEnemySeen != null) {
                Direction spawnDir = allyBase.directionTo(lastEnemy).opposite();
                if (uc.canSpawn(t, spawnDir)) {
                    uc.spawn(t, spawnDir);
                    return true;
                }
            }

            int random = (int)(uc.getRandomDouble() * index);

            uc.spawn(t, myDirs[random]);
            roundSpawn = round;
            return true;
        } else {
            int lowestDamage = 10000;
            Direction bestDir = null;

            for (Direction dir : myDirs) {
                if (dir == null) continue;
                int damage = 0;
                for (UnitInfo enemy : enemies) {
                    Location loc = enemy.getLocation();
                    if (move.isObstructed(loc, myLoc.add(dir))) continue;
                    if (enemy.getStat(UnitStat.ATTACK_RANGE) >= loc.distanceSquared(myLoc.add(dir))) {
                        damage += enemy.getStat(UnitStat.ATTACK);
                    }
                }

                if (damage < lowestDamage) {
                    lowestDamage = damage;
                    bestDir = dir;
                }
            }

            if (bestDir != null && lowestDamage <= 45) {
                uc.spawn(t, bestDir);
                roundSpawn = round;
                return true;
            }
        }

        return false;
    }

    int senseDanger(UnitInfo[] enemies, UnitInfo[] neutrals) {
        int enemyCounterIn = 0;
        int enemyCounterOut = 0;
        int dangerLevel;

        for (int i=0; i<enemies.length; i++) {
            Location enemyLoc = enemies[i].getLocation();
            Boolean isObstructed = move.isObstructed(enemyLoc);
            if (allyBase.distanceSquared(enemyLoc) <= uc.getInfo().getStat(UnitStat.ATTACK_RANGE) && !isObstructed) enemyCounterIn++;
            else if (!isObstructed) enemyCounterOut++;
        }
        for (int i=0; i<neutrals.length; i++) {
            Location enemyLoc = neutrals[i].getLocation();
            Boolean isObstructed = move.isObstructed(enemyLoc);
            if (allyBase.distanceSquared(enemyLoc) <= uc.getInfo().getStat(UnitStat.ATTACK_RANGE) && !isObstructed) enemyCounterIn++;
            else if (!isObstructed) enemyCounterOut++;
        }

        if (enemyCounterIn > 0) {
            dangerLevel = constants.BASE_HEAVILY_ATTACKED;
        } else if (enemyCounterOut > 1) {
            dangerLevel = constants.BASE_ATTACKED;
        } else {
            dangerLevel = constants.BASE_SAFE;
        }

        manager.setBaseDanger(dangerLevel);
        return dangerLevel;
    }

    void senseBase(){
        UnitInfo[] units = uc.senseUnits(opponent);
        for (UnitInfo unit: units) {
            if (unit.getType() == UnitType.BASE) {
                enemyBase = unit.getLocation();
                uc.writeOnSharedArray(constants.ENEMY_BASE_POSITION, helper.realLocationToInt(enemyBase.x, enemyBase.y));
                isBaseClose = true;
            }
        }

        if (!isBaseClose) {
            safeSpawn = Direction.values();
        } else {
            int index = 0;
            float range = UnitType.BASE.getStat(UnitStat.ATTACK_RANGE);
            Direction[] myDirs = Direction.values();
            Direction[] tempDirs = new Direction[9];
            for (Direction dir: myDirs) {
                Location baseTarget = allyBase.add(dir);
                if (baseTarget.distanceSquared(enemyBase) > range || uc.isObstructed(baseTarget, enemyBase)) {
                    tempDirs[index] = dir;
                    index++;
                }
            }

            safeSpawn = new Direction[index];

            index = 0;
            for (Direction dir: tempDirs) {
                if (dir == null) break;
                safeSpawn[index] = dir;
                index++;
            }
        }
    }
}
