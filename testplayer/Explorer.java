package testplayer;

import aic2022.user.Location;
import aic2022.user.Team;
import aic2022.user.UnitController;
import aic2022.user.UnitInfo;

public class Explorer extends MyUnit {

    ExplorerPathfinder pathfinder;

    Explorer(UnitController uc){
        super(uc);

        this.pathfinder = new ExplorerPathfinder(uc);
    }

    void playRound(){
        uc.writeOnSharedArray(constants.EXPLORER_ALIVE, 1);
        if (!inDungeon && uc.senseUnits(Team.NEUTRAL).length != 0 && round < 100) uc.writeOnSharedArray(constants.NEUTRAL_UNITS, 1);

        tryUseSkill(uc.getLocation());
        genericBehaviour(false);
        tryLvlUp(enemies, neutrals);

        attack.genericTryAttack(enemies);
        attack.genericTryAttack(neutrals);
        if (uc.canMove()) {
            tryMove(enemies, neutrals, false);
        }
        attack.genericTryAttack(uc.senseUnits(opponent));
        attack.genericTryAttack(uc.senseUnits(Team.NEUTRAL));
        attack.tryAttackShrine(uc.senseShrines());

        uc.writeOnSharedArray(constants.EXPLORER_ALIVE, 1);

        finish();
    }

    void tryUseSkill(Location loc) {
        if (uc.canUseSecondAbility(loc)) uc.useSecondAbility(loc);
    }

    void tryLvlUp(UnitInfo[] enemies, UnitInfo[] neutrals) {
        if (uc.canLevelUp() && ((enemies.length == 0 && neutrals.length == 0) && uc.getInfo().getLevel() < 3) && uc.readOnSharedArray(constants.ASSASSIN_LVL3) == 1) {
            uc.levelUp();
        }
    }

    void tryMove(UnitInfo[] enemies, UnitInfo[] neutrals, boolean reckless) {
        if (uc.readOnSharedArray(constants.DESTROY_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, true, false);
        } else if (uc.readOnSharedArray(constants.GO_TO_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, false, false);
        }

        Location globalShrine = manager.getClosestShrine();
        if (globalShrine != null && uc.canSenseLocation(globalShrine)) globalShrine = null;

        int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
        if (position != 0) {
            enemyBase = helper.intToRealLocation(uc.readOnSharedArray(constants.ENEMY_BASE_POSITION));
        }

        if (neutrals.length > 0 && !inDungeon && (closestChest == null || !canSurviveReachChest(closestChest.getLocation()))) {
            move.kiteToBase(neutrals);
        }

        if (closestChest != null) {
            if (canSurviveReachChest(closestChest.getLocation())) {
                pathfinder.getNextLocationTarget(closestChest.getLocation(), true, true);
                getChest();
            } else if (inDungeon) {
                mustExitDungeon = true;
                doDungeon();
                if (inDungeon) pathfinder.getNextLocationTarget(closestDungeon, true, true);
            } else {
                doDefault(false);
            }
            resetExplore = true;
        }
        else if (closestShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(closestShrine.getLocation(), reckless, false);
            resetExplore = true;
        }
        else if (globalShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(globalShrine, reckless, false);
            resetExplore = true;
        }
        else if (obstructedChest != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(obstructedChest.getLocation(), false, true);
            getChest();
            resetExplore = true;
        }
        else if (closestDungeon != null) {
            if (!inDungeon && canEnterDungeon) {
                doDungeon();
                if (!inDungeon) pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
                else doDungeon();
                resetExplore = true;
            } else if (inDungeon && mustExitDungeon) {
                doDungeon();
                if (inDungeon) pathfinder.getNextLocationTarget(closestDungeon, true, true);
                else doDungeon();
                resetExplore = true;
            } else {
                doDefault(reckless);
            }
        } else {
            doDefault(reckless);
        }

        doDungeon();
    }

    void doDefault(Boolean reckless) {
        pathfinder.getNextLocationTarget(move.explore(resetExplore), reckless, true);
        resetExplore = false;
    }
}
