package testplayer;

import aic2022.user.Location;
import aic2022.user.UnitController;

public class Helper {
    UnitController uc;
    private final Constants constants = new Constants();

    Helper(UnitController uc){
        this.uc = uc;
    }

    public int realLocationToInt(int locX, int locY) {
        return locX * 1100 + locY;
    }

    public Location intToRealLocation(int number) {
        return new Location(number / 1100, number % 1100);
    }

    public int locationToInt(int locX, int locY) {
        Location allyBase = intToRealLocation(uc.readOnSharedArray(constants.BASE_POSITION));
        return (locX - allyBase.x + 80) * 100 + (locY - allyBase.y + 80);
    }

    public int locationToInt(Location loc) {
        return locationToInt(loc.x, loc.y);
    }

    public Location intToLocation(int number) {
        Location allyBase = intToRealLocation(uc.readOnSharedArray(constants.BASE_POSITION));
        return new Location((number / 100) + allyBase.x - 80, (number % 100) + allyBase.y - 80);
    }
}