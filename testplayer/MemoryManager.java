package testplayer;

import aic2022.user.*;

public class MemoryManager {

    private final UnitController uc;
    private final Constants constants = new Constants();
    private final Helper helper;

    // GENERAL

    MemoryManager(UnitController uc){
        this.uc = uc;
        this.helper = new Helper(uc);
    }

    // MAP FUNCTIONS

    public int getIndexMap(int locX, int locY) {
        return constants.ID_MAP_INFO + helper.locationToInt(locX, locY) * constants.INFO_PER_CELL;
    }

    public int getIndexMap(Location loc) {
        return constants.ID_MAP_INFO + helper.locationToInt(loc) * constants.INFO_PER_CELL;
    }

    public Location getAllyBase() {
        int position = uc.readOnSharedArray(constants.BASE_POSITION);
        return helper.intToRealLocation(position);
    }

    public void setPortalSafe(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index, constants.SAFE_PORTAL);
    }

    public void setPortalDangerous(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index, constants.DANGEROUS_PORTAL);
    }

    public int getPortalState(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index);
    }

    public void addChestDungeon(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 1, uc.readOnSharedArray(index + 1) + 1);
    }

    public int getChestsDungeon(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index + 1);
    }

    public void setDungeonCleared(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 2, 1);
    }

    public void resetDungeon(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 2, 0);
    }

    public boolean isDungeonCleared(Location loc) {
        int index = getIndexMap(loc);
        if (uc.readOnSharedArray(index + 2) == 1) return true;
        return false;
    }

    public void resetDungeons() {
        for (int i = constants.DUNGEON_LOCATION; i < constants.DUNGEON_LOCATION + constants.DUNGEON_MAX; i++) {
            int position = uc.readOnSharedArray(i);

            if (position != 0) {
                Location dungeonLoc = helper.intToRealLocation(uc.readOnSharedArray(i));
                resetDungeon(dungeonLoc);
            }
        }
    }

    public Location getClosestSafeDungeon() {
        int bestChests = 0;
        Location bestLoc = null;

        for (int i = constants.DUNGEON_LOCATION; i < constants.DUNGEON_LOCATION + constants.DUNGEON_MAX; i++) {
            int position = uc.readOnSharedArray(i);

            if (position != 0) {
                Location dungeonLoc = helper.intToRealLocation(uc.readOnSharedArray(i));
                int state = getPortalState(dungeonLoc);
                int chests = getChestsDungeon(dungeonLoc);
                boolean isCleared = isDungeonCleared(dungeonLoc);
                if (state == 1 && chests > 0 && !isCleared) {
                    if (bestLoc == null || bestChests < chests) bestLoc = dungeonLoc;
                }
            }
        }

        return bestLoc;
    }

    public boolean safeDungeons() {
        for (int i = constants.DUNGEON_LOCATION; i < constants.DUNGEON_LOCATION + constants.DUNGEON_MAX; i++) {
            int position = uc.readOnSharedArray(i);

            if (position != 0) {
                Location dungeonLoc = helper.intToRealLocation(uc.readOnSharedArray(i));
                int state = getPortalState(dungeonLoc);
                if (state == 1) return true;
            }
        }
        return false;
    }

    public void setBaseDanger(int dangerLevel) {
        uc.writeOnSharedArray(constants.BASE_DANGER_LEVEL, dangerLevel);
    }

    public int getBaseDanger() {
        return uc.readOnSharedArray(constants.BASE_DANGER_LEVEL);
    }

    public void setShrineNeutral(Location loc) {
        Location myLoc = uc.getLocation();

        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 1, constants.NEUTRAL_SHRINE);

        int counter = 0;
        UnitInfo[] enemies = uc.senseUnits(Team.NEUTRAL);
        for (UnitInfo enemy : enemies) {
            if (!uc.isObstructed(myLoc, enemy.getLocation())) counter++;
        }

        addShrine(loc);
        if (counter < 2) setShrineSafe(loc);
        else setShrineDangerous(loc);
    }

    public void setShrineAllied(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 1, constants.ALLIED_SHRINE);
        setShrineLastSeen(loc);
    }

    public void setShrineEnemy(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 1, constants.ENEMY_SHRINE);
        addShrine(loc);
    }

    public void setShrineSafe(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 2, constants.SAFE_SHRINE);
    }

    public void setShrineDangerous(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 2, constants.DANGEROUS_SHRINE);
    }

    public void setShrineLastSeen(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 3, uc.getRound());
    }

    public int getShrineLastSeen(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index + 3);
    }

    public int getShrineState(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index + 1);
    }

    public int getShrineSafety(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index + 2);
    }

    public void setObstructedChest(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index, constants.OBSTRUCTED_CHEST);
    }

    public int getObstructedChest(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index);
    }

    public void addAssassin(UnitInfo assassin) {
        Location loc = assassin.getLocation();
        int id = assassin.getID();
        int position = helper.realLocationToInt(loc.x, loc.y);
        int index = -1;
        int emptyIndex = -1;

        for (int i = constants.ASSASSIN_LIST; i < constants.ASSASSIN_LIST + (constants.ASSASSIN_INFO_PER_CELL * constants.MAX_ASSASSIN); i += constants.ASSASSIN_INFO_PER_CELL) {
            int read = uc.readOnSharedArray(i + constants.ASSASSIN_ID);
            if (read == id) {
                index = i;
                break;
            }
            if (read == 0 && emptyIndex == -1) {
                emptyIndex = i;
            }
        }

        if (index == -1 && emptyIndex != -1) {
            uc.writeOnSharedArray(emptyIndex, position);
            uc.writeOnSharedArray(emptyIndex + constants.ASSASSIN_ID, id);
            uc.writeOnSharedArray(emptyIndex + constants.ASSASSIN_HP, assassin.getHealth());
            uc.writeOnSharedArray(emptyIndex + constants.ASSASSIN_ROUND, uc.getRound());
            int root = (int) assassin.getCurrentMovementCooldown() - 1;
            uc.writeOnSharedArray(emptyIndex + constants.ASSASSIN_ROOT_LEFT, root);
            uc.writeOnSharedArray(emptyIndex + constants.ASSASSIN_DEFENSE, (int) assassin.getStat(UnitStat.DEFENSE));
        } else if (index != -1) {
            uc.writeOnSharedArray(index, position);
            uc.writeOnSharedArray(index + constants.ASSASSIN_HP, id);
            uc.writeOnSharedArray(index + constants.ASSASSIN_ROUND, uc.getRound());
            int root = (int) assassin.getCurrentMovementCooldown() - 1;
            uc.writeOnSharedArray(index + constants.ASSASSIN_ROOT_LEFT, root);
            uc.writeOnSharedArray(index + constants.ASSASSIN_DEFENSE, (int) assassin.getStat(UnitStat.DEFENSE));
        }
    }

    public void updateAssassin(int id, int damage, int root) {
        int index = -1;

        for (int i = constants.ASSASSIN_LIST; i < constants.ASSASSIN_LIST + (constants.ASSASSIN_INFO_PER_CELL * constants.MAX_ASSASSIN); i += constants.ASSASSIN_INFO_PER_CELL) {
            int read = uc.readOnSharedArray(i + constants.ASSASSIN_ID);
            if (read == id) {
                index = i;
                break;
            }
        }

        if (index != -1) {
            int newHP = uc.readOnSharedArray(index + constants.ASSASSIN_HP) - damage;
            if (newHP <= 0) {
                uc.writeOnSharedArray(index, 0);
                uc.writeOnSharedArray(index + constants.ASSASSIN_ID, 0);
                uc.writeOnSharedArray(index + constants.ASSASSIN_HP, 0);
                uc.writeOnSharedArray(index + constants.ASSASSIN_DEFENSE, 0);
                uc.writeOnSharedArray(index + constants.ASSASSIN_ROOT_LEFT, 0);
                uc.writeOnSharedArray(index + constants.ASSASSIN_ROUND, 0);
            } else {
                uc.writeOnSharedArray(index + constants.ASSASSIN_HP, newHP);
                uc.writeOnSharedArray(index + constants.ASSASSIN_ROOT_LEFT, uc.readOnSharedArray(index + constants.ASSASSIN_ROOT_LEFT) + root);
            }
        }
    }

    public void addShrine(Location loc) {
        int position = helper.realLocationToInt(loc.x, loc.y);
        boolean found = false;
        int emptyIndex = -1;

        for (int i = constants.SHRINE_LOCATION; i < constants.SHRINE_LOCATION + constants.SHRINE_MAX; i++) {
            int read = uc.readOnSharedArray(i);
            if (read == position) found = true;
            if (read == 0 && emptyIndex == -1) {
                emptyIndex = i;
            }
        }

        if (!found && emptyIndex != -1) {
            uc.writeOnSharedArray(emptyIndex, position);
            uc.writeOnSharedArray(constants.HAS_SHRINES, 1);
        }
    }

    public Location getClosestShrine() {
        Location myLoc = uc.getLocation();
        int minDistance = 10000000;
        Location closestShrine = null;
        for (int i = constants.SHRINE_LOCATION; i < constants.SHRINE_LOCATION + constants.SHRINE_MAX; i++) {
            int position = uc.readOnSharedArray(i);

            if (position != 0) {
                Location shrineLoc = helper.intToRealLocation(uc.readOnSharedArray(i));
                int state = getShrineState(shrineLoc);

                if (state == constants.ALLIED_SHRINE && getShrineLastSeen(shrineLoc) + 75 > uc.getRound()) continue;

                int distance = myLoc.distanceSquared(shrineLoc);

                if (distance < minDistance) {
                    minDistance = distance;
                    closestShrine = shrineLoc;
                }
            }
        }

        return closestShrine;
    }

    public void addDungeon(Location loc) {
        int position = helper.realLocationToInt(loc.x, loc.y);
        boolean found = false;
        int emptyIndex = -1;

        for (int i = constants.DUNGEON_LOCATION; i < constants.DUNGEON_LOCATION + constants.DUNGEON_MAX; i++) {
            int read = uc.readOnSharedArray(i);
            if (read == position) found = true;
            if (read == 0 && emptyIndex == -1) {
                emptyIndex = i;
            }
        }

        if (!found && emptyIndex != -1) {
            uc.writeOnSharedArray(emptyIndex, position);
        }
    }

    public boolean getRangerFlag() {
        if (uc.readOnSharedArray(constants.RANGERS_LVL2) == 1) return true;
        return false;
    }

    public boolean getKnightFlag() {
        if (uc.readOnSharedArray(constants.KNIGHTS_LVL2) == 1) return true;
        return false;
    }
}