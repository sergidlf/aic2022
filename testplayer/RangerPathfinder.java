package testplayer;

import aic2022.user.*;

public class RangerPathfinder {

    UnitController uc;
    public final Constants constants = new Constants();
    Helper helper;
    final int INF = 1000000000;
    boolean rotateRight = true; //if I should rotate right or left
    boolean rotate = false;
    Location lastObstacleFound = null; //latest obstacle I've found in my way
    int minDistToEnemy = INF; //minimum distance I've been to the enemy while going around an obstacle
    Location prevTarget = null; //previous target
    Location myLoc;
    Location enemyBase = null;
    Team myTeam;
    UnitInfo[] enemies;
    UnitInfo[] enemies2;
    Direction microDir;
    Direction[] myDirs;
    float baseRange;
    boolean isEnemies;
    UnitType myType;

    RangerPathfinder(UnitController uc){
        this.myDirs = Direction.values();
        this.uc = uc;
        this.myTeam = uc.getTeam();
        this.baseRange = UnitType.BASE.getStat(UnitStat.ATTACK_RANGE);
        this.myType = uc.getType();
        this.helper = new Helper(uc);
    }

    void setEnemyBase(Location target) {
        enemyBase = target;
    }

    Boolean getNextLocationTarget(Location target, boolean reckless, boolean reachTarget){
        if (enemyBase == null) {
            int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
            if (position != 0) setEnemyBase(helper.intToRealLocation(position));
        }

        if (!uc.canMove()) return false;
        if (target == null) return false;
        isEnemies = false;

        //different target? ==> previous data does not help!
        if (prevTarget == null || !target.isEqual(prevTarget)) resetPathfinding();

        //If I'm at a minimum distance to the target, I'm free!
        myLoc = uc.getLocation();
        int d = myLoc.distanceSquared(target);
        if (d <= minDistToEnemy) resetPathfinding();

        //Update data
        prevTarget = target;
        minDistToEnemy = Math.min(d, minDistToEnemy);

        //If there's an obstacle I try to go around it [until I'm free] instead of going to the target directly
        Direction dir = myLoc.directionTo(target);
        if (lastObstacleFound != null) dir = myLoc.directionTo(lastObstacleFound);

        //This should not happen for a single unit, but whatever
        if (uc.canMove(dir)) resetPathfinding();

        //I rotate clockwise or counterclockwise (depends on 'rotateRight'). If I try to go out of the map I change the orientation
        //Note that we have to try at most 16 times since we can switch orientation in the middle of the loop. (It can be done more efficiently)
        if (!reckless) doMicro();
        else microDir = null;

        if (microDir != null) {
            if (microDir != Direction.ZERO) uc.move(microDir);
            return true;
        }
        else if (!isEnemies) {
            if (!reachTarget) {
                int currentDistance = myLoc.distanceSquared(target);
                if (currentDistance <= myType.getStat(UnitStat.ATTACK_RANGE)) return false;
                else if (currentDistance < myType.getStat(UnitStat.MIN_ATTACK_RANGE)) {
                    if (uc.canMove(myLoc.directionTo(target).opposite())) {
                        uc.move(myLoc.directionTo(target).opposite());
                        return true;
                    }
                }
            }
            for (int i = 0; i < 16; ++i) {
                for (int j = 0; j < myDirs.length; j++) {
                    if (myDirs[j] == dir) {
                        Location loc = myLoc.add(dir);
                        if (uc.canMove(dir) && ((!isEnemies && (enemyBase == null || (loc.distanceSquared(enemyBase) > baseRange) || (uc.canSenseLocation(enemyBase) && uc.isObstructed(loc, enemyBase)))) || reckless)) {
                            uc.move(dir);
                            return true;
                        }
                        break;
                    }
                }
                if (!rotate && myLoc.add(dir.rotateLeft()).distanceSquared(target) > myLoc.add(dir.rotateRight()).distanceSquared(target)) {
                    rotateRight = true;
                    rotate = true;
                }
                Location newLoc = myLoc.add(dir);
                if (uc.isOutOfMap(newLoc)) rotateRight = !rotateRight;
                    //If I could not go in that direction and it was not outside of the map, then this is the latest obstacle found
                else lastObstacleFound = myLoc.add(dir);
                if (rotateRight) dir = dir.rotateRight();
                else dir = dir.rotateLeft();
            }

            for (int j = 0; j < myDirs.length; j++) {
                if (myDirs[j] == dir) {
                    Location loc = myLoc.add(dir);
                    if (uc.canMove(dir) && ((!isEnemies && (enemyBase == null || (loc.distanceSquared(enemyBase) > baseRange) || (uc.canSenseLocation(enemyBase) && uc.isObstructed(loc, enemyBase)))) || reckless)) {
                        uc.move(dir);
                        return true;
                    }
                    break;
                }
            }
        }

        return false;
    }

    void resetPathfinding(){
        lastObstacleFound = null;
        minDistToEnemy = INF;
    }

    public void doMicro() {
        MicroInfo[] microInfo = new MicroInfo[9];
        enemies = uc.senseUnits(myTeam.getOpponent());
        enemies2 = uc.senseUnits(Team.NEUTRAL);
        for (int i = 0; i < 9; i++) {
            if (!uc.canMove(myDirs[i])) continue;
            Location target = myLoc.add(myDirs[i]);
            microInfo[i] = new MicroInfo(myLoc.add(myDirs[i]));

            if (enemyBase != null && target.distanceSquared(enemyBase) <= baseRange) {
                if (uc.canSenseLocation(enemyBase) && !uc.isObstructed(target, enemyBase) || !uc.canSenseLocation(enemyBase)) microInfo[i].damage += 2000;
            }

            for (int j = 0; j < enemies.length; j++) {
                Location enemyLoc = enemies[j].getLocation();
                if (uc.canSenseLocation(enemyLoc) && uc.canSenseLocation(target) && uc.isObstructed(enemyLoc, target)) continue;
                if (!uc.canSenseLocation(target)) continue;
                isEnemies = true;
                UnitInfo enemy = enemies[j];
                int distance = microInfo[i].loc.distanceSquared(enemy.getLocation());
                microInfo[i].updateSafe(distance, enemy);
            }

            for (int j = 0; j < enemies2.length; j++) {
                Location enemyLoc = enemies2[j].getLocation();
                if (uc.canSenseLocation(enemyLoc) && uc.canSenseLocation(target) && uc.isObstructed(enemyLoc, target)) continue;
                if (!uc.canSenseLocation(target)) continue;
                isEnemies = true;
                UnitInfo enemy = enemies2[j];
                int distance = microInfo[i].loc.distanceSquared(enemy.getLocation());
                microInfo[i].updateSafe(distance, enemy);
            }
        }

        int bestIndex = 8;

        for (int i = 8; i >= 0; i--) {
            if (!uc.canMove(myDirs[i])) continue;
            if (!microInfo[bestIndex].isBetter(microInfo[i])) bestIndex = i;
        }

        if (isEnemies) {
            microDir = myDirs[bestIndex];
        } else microDir = null;
    }

    class MicroInfo {
        int damage;
        int softdamage;
        int minDistToEnemy;
        Location loc;

        public MicroInfo(Location loc) {
            this.loc = loc;
            damage = 0;
            softdamage = 0;
            minDistToEnemy = 100000;
        }

        void updateSafe(int distance, UnitInfo enemy) {
            UnitType enemyType = enemy.getType();
            boolean canMove = enemy.getCurrentMovementCooldown() < 2;

            if (enemyType == UnitType.BARBARIAN) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                    softdamage += enemy.getStat(UnitStat.ATTACK);
                }
                else if (distance < 19 && canMove) softdamage += 58;
            } else if (enemyType == UnitType.RANGER) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE) && distance >= enemy.getStat(UnitStat.MIN_ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                    softdamage += enemy.getStat(UnitStat.ATTACK);
                }
                else if (distance < 51 && distance >= 5 && canMove) softdamage += 33;
            } else if (enemyType == UnitType.KNIGHT) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                    softdamage += enemy.getStat(UnitStat.ATTACK);
                }
                else if (distance < 14 && canMove) softdamage += 36;
            } else if (enemyType == UnitType.BASE) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                    softdamage += enemy.getStat(UnitStat.ATTACK);
                }
            } else if (enemyType == UnitType.EXPLORER) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                    softdamage += enemy.getStat(UnitStat.ATTACK);
                }
                else if (distance < 26 && canMove) softdamage += 10;
            } else if (enemyType == UnitType.MAGE) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                    softdamage += enemy.getStat(UnitStat.ATTACK);
                }
                else if (distance < 33 && canMove) softdamage += 108;
            } else if (enemyType == UnitType.ASSASSIN) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) {
                    damage += enemy.getStat(UnitStat.ATTACK);
                    softdamage += enemy.getStat(UnitStat.ATTACK);
                }
                else if (distance < 9 && canMove) softdamage += 148;
            }

            if (distance < minDistToEnemy) minDistToEnemy = distance;
        }

        boolean canAttack() {
            return myType.getStat(UnitStat.ATTACK_RANGE) >= minDistToEnemy;
        }

        boolean tooClose() {
            return minDistToEnemy < myType.getStat(UnitStat.MIN_ATTACK_RANGE);
        }

        boolean isBetter(MicroInfo m) {
            if (damage > 1900) return false;
            if (m.damage > 1900) return true;

            if (uc.canAttack()) {
                if (canAttack()) {
                    if (!tooClose()) {
                        if (!m.canAttack() || m.tooClose()) return true;
                    }
                    if (m.canAttack() && !m.tooClose()) return false;
                    return minDistToEnemy >= m.minDistToEnemy;
                }
                if (m.canAttack() && !tooClose()) return false;
                if (damage <= 0 && m.damage <= 0) {
                    if (softdamage <= 0 && m.softdamage <= 0 && uc.getInfo().getCurrentAbilityICooldown() == 0) return minDistToEnemy <= m.minDistToEnemy;
                    return softdamage <= m.softdamage;
                }
                return damage <= m.damage;
            }

            if (damage <= 0 && m.softdamage <= 0) {
                if (softdamage <= 0 && m.softdamage <= 0) {
                    if (minDistToEnemy <= 32 && m.minDistToEnemy <= 32) return minDistToEnemy >= m.minDistToEnemy;
                    if (minDistToEnemy > 32) return false;
                    return true;
                }
            }

            return minDistToEnemy >= m.minDistToEnemy;
        }
    }
}