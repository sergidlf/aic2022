package testplayer;

import aic2022.user.Location;
import aic2022.user.Team;
import aic2022.user.UnitController;
import aic2022.user.UnitInfo;

public class Barbarian extends MyUnit {

    BarbarianPathfinder pathfinder;

    Barbarian(UnitController uc){
        super(uc);

        this.pathfinder = new BarbarianPathfinder(uc);
    }

    void playRound(){
        genericBehaviour(false);

        attack.genericTryAttack(enemies);
        attack.genericTryAttack(neutrals);
        if (uc.canMove()) {
            tryMove(enemies, neutrals, false);
        }
        attack.genericTryAttack(uc.senseUnits(opponent));
        attack.genericTryAttack(uc.senseUnits(Team.NEUTRAL));
        attack.tryAttackShrine(uc.senseShrines());

        finish();
    }

    void tryMove(UnitInfo[] enemies, UnitInfo[] neutrals, boolean reckless) {
        if (uc.readOnSharedArray(constants.DESTROY_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, true, false);
        } else if (uc.readOnSharedArray(constants.GO_TO_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, false, false);
        }

        Location globalShrine = manager.getClosestShrine();
        if (globalShrine != null)  {
            boolean isDangerous = manager.getShrineSafety(globalShrine) == constants.DANGEROUS_SHRINE;

            if (!isDangerous) noAttackRound = 0;
            if (uc.canSenseLocation(globalShrine)) globalShrine = null;
        }

        if (round < noAttackRound && enemies.length == 0 && neutrals.length == 0) return;

        int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
        if (position != 0) {
            enemyBase = helper.intToRealLocation(uc.readOnSharedArray(constants.ENEMY_BASE_POSITION));
        }

        if (round < 400 && neutrals.length > 0) {
            move.kiteToBase(neutrals);
        }

        if (closestChest != null) {
            pathfinder.getNextLocationTarget(closestChest.getLocation(), true, true);
            getChest();
            resetExplore = true;
        }
        else if (closestShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(closestShrine.getLocation(), reckless, false);
            resetExplore = true;
        }
        else if (obstructedChest != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(obstructedChest.getLocation(), false, true);
            getChest();
            resetExplore = true;
        }
        else if (closestDungeon != null) {
            if (!inDungeon && canEnterDungeon && !mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
                resetExplore = true;
            } else if (inDungeon && mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, true, true);
                resetExplore = true;
            } else {
                doDefault(globalShrine, reckless);
            }
        } else {
            doDefault(globalShrine, reckless);
        }
    }

    void doDefault(Location shrine, Boolean reckless) {
        if (shrine != null) {
            pathfinder.getNextLocationTarget(shrine, reckless, false);
            resetExplore = true;
        }
        else {
            pathfinder.getNextLocationTarget(move.explore(resetExplore), reckless, true);
            resetExplore = false;
        }
    }}
