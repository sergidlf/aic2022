package testplayer;

import aic2022.user.*;

public class Assassin extends MyUnit {

    AssassinPathfinder pathfinder;

    Assassin(UnitController uc){
        super(uc);

        this.pathfinder = new AssassinPathfinder(uc);
    }

    void playRound(){
        genericBehaviour(true);

        uc.writeOnSharedArray(constants.ASSASSIN_ALIVE, uc.readOnSharedArray(constants.ASSASSIN_ALIVE) + 1);

        tryLvlUp(enemies, neutrals);
        attack.assassinAttack(enemies, neutrals);
        if (uc.readOnSharedArray(constants.DESTROY_BASE) == 1) attack.assassinAttackBase(enemyBase);
        tryMove(false, enemies, neutrals);

        attack.tryAttackShrineAssassin(uc.senseShrines(), uc.senseUnits(opponent), uc.senseUnits(Team.NEUTRAL));

        finish();
    }

    void tryLvlUp(UnitInfo[] enemies, UnitInfo[] neutrals) {
        if (uc.canLevelUp() && ((enemies.length == 0 && neutrals.length == 0) || uc.getInfo().getLevel() < 3)) {
            uc.levelUp();
        }
    }

    void tryMove(boolean reckless, UnitInfo[] enemies, UnitInfo[] neutrals) {
        if (uc.readOnSharedArray(constants.DESTROY_BASE) == 1) {
            if (uc.getLocation().distanceSquared(enemyBase) > 8) pathfinder.getNextLocationTarget(enemyBase, true, false);
        } else if (uc.readOnSharedArray(constants.GO_TO_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, false, false);
        }

        Location globalShrine = manager.getClosestShrine();
        if (globalShrine != null)  {
            boolean isDangerous = manager.getShrineSafety(globalShrine) == constants.DANGEROUS_SHRINE;

            if (!isDangerous) noAttackRound = 0;
            if (uc.canSenseLocation(globalShrine)) globalShrine = null;
        }

        int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
        if (position != 0) {
            enemyBase = helper.intToRealLocation(uc.readOnSharedArray(constants.ENEMY_BASE_POSITION));
        }

        if (closestChest != null) {
            if (!tryShadowStep(closestChest.getLocation(), enemies, neutrals)) pathfinder.getNextLocationTarget(closestChest.getLocation(), reckless, true);
            getChest();
            resetExplore = true;
        }
        else if (closestShrine != null && !mustExitDungeon) {
            if (uc.senseUnits((int)uc.getInfo().getStat(UnitStat.VISION_RANGE), myTeam, true).length == 0) {
                tryShadowStep(closestShrine.getLocation(), enemies, neutrals);
            }
            pathfinder.getNextLocationTarget(closestShrine.getLocation(), reckless, false);
            resetExplore = true;
        }
        else if (globalShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(globalShrine, reckless, false);
            resetExplore = true;
        }
        else if (globalEnemy != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(globalEnemy, reckless, false);
            resetExplore = true;
        }
        else if (obstructedChest != null && !mustExitDungeon) {
            if (!tryShadowStep(obstructedChest.getLocation(), enemies, neutrals)) pathfinder.getNextLocationTarget(obstructedChest.getLocation(), reckless, true);
            getChest();
            resetExplore = true;
        }
        else if (closestDungeon != null) {
            if (!inDungeon && canEnterDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
                resetExplore = true;
            } else if (inDungeon && mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, true, true);
                resetExplore = true;
            } else {
                doDefault(reckless);
            }
        } else {
            doDefault(reckless);
        }
    }

    void doDefault(Boolean reckless) {
        pathfinder.getNextLocationTarget(move.explore(resetExplore), reckless, true);
        resetExplore = false;
    }

    boolean tryShadowStep(Location loc, UnitInfo[] enemies, UnitInfo[] neutrals) {
        if (!uc.canMove()) return false;

        Location myLoc = uc.getLocation();
        Direction[] dirs = Direction.values();

        for (Direction dir : dirs) {
            Location target = loc.add(dir);
            if (uc.canUseFirstAbility(target)) {

                for (UnitInfo enemy : enemies) {
                    Location enemyLoc = enemy.getLocation();
                    int distance = target.distanceSquared(enemyLoc);
                    if (distance <= 2) return false;
                }

                for (UnitInfo enemy : neutrals) {
                    Location enemyLoc = enemy.getLocation();
                    int distance = target.distanceSquared(enemyLoc);
                    if (distance <= 2) return false;
                }

                if (uc.canMove(myLoc.directionTo(target)) && myLoc.distanceSquared(target) <= 2) return false;
                uc.useFirstAbility(target);
                return true;
            }
        }

        return false;
    }
}