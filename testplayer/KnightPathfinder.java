package testplayer;

import aic2022.user.*;

public class KnightPathfinder {

    UnitController uc;
    public final Constants constants = new Constants();
    Helper helper;
    final int INF = 1000000000;
    boolean rotateRight = true; //if I should rotate right or left
    boolean rotate = false;
    Location lastObstacleFound = null; //latest obstacle I've found in my way
    int minDistToEnemy = INF; //minimum distance I've been to the enemy while going around an obstacle
    Location prevTarget = null; //previous target
    Location myLoc;
    Location enemyBase = null;
    Team myTeam;
    UnitInfo[] enemies;
    UnitInfo[] enemies2;
    Direction microDir;
    Direction[] myDirs;
    float baseRange;
    boolean isEnemies;
    UnitType myType;
    Boolean isFleeing = false;

    KnightPathfinder(UnitController uc){
        this.myDirs = Direction.values();
        this.uc = uc;
        this.myTeam = uc.getTeam();
        this.baseRange = UnitType.BASE.getStat(UnitStat.ATTACK_RANGE);
        this.myType = uc.getType();
        this.helper = new Helper(uc);
    }

    void setEnemyBase(Location target) {
        enemyBase = target;
    }

    Boolean getNextLocationTarget(Location target, boolean reckless, boolean reachTarget){
        if (enemyBase == null) {
            int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
            if (position != 0) setEnemyBase(helper.intToRealLocation(position));
        }

        if (uc.getInfo().getLevel() >= 2 && uc.getInfo().getCurrentMovementCooldown() >= 1 && uc.getInfo().getCurrentAbilityICooldown() < 1) {
            doMicro(true);
            if (microDir != null && microDir != Direction.ZERO && uc.canUseFirstAbility(uc.getLocation().add(microDir))) uc.useFirstAbility(uc.getLocation().add(microDir));
            microDir = null;
        }

        if (!uc.canMove()) return false;
        if (target == null) return false;
        isEnemies = false;

        //different target? ==> previous data does not help!
        if (prevTarget == null || !target.isEqual(prevTarget)) resetPathfinding();

        //If I'm at a minimum distance to the target, I'm free!
        myLoc = uc.getLocation();
        int d = myLoc.distanceSquared(target);
        if (d <= minDistToEnemy) resetPathfinding();

        //Update data
        prevTarget = target;
        minDistToEnemy = Math.min(d, minDistToEnemy);

        //If there's an obstacle I try to go around it [until I'm free] instead of going to the target directly
        Direction dir = myLoc.directionTo(target);
        if (lastObstacleFound != null) dir = myLoc.directionTo(lastObstacleFound);

        //This should not happen for a single unit, but whatever
        if (uc.canMove(dir)) resetPathfinding();

        //I rotate clockwise or counterclockwise (depends on 'rotateRight'). If I try to go out of the map I change the orientation
        //Note that we have to try at most 16 times since we can switch orientation in the middle of the loop. (It can be done more efficiently)
        if (!reckless) doMicro(false);
        else microDir = null;

        if (microDir != null) {
            if (microDir != Direction.ZERO) uc.move(microDir);
            return true;
        }
        else if (!isEnemies) {
            if (!reachTarget) {
                int currentDistance = myLoc.distanceSquared(target);
                if (currentDistance <= myType.getStat(UnitStat.ATTACK_RANGE)) return false;
                else if (currentDistance < myType.getStat(UnitStat.MIN_ATTACK_RANGE)) {
                    if (uc.canMove(myLoc.directionTo(target).opposite())) {
                        uc.move(myLoc.directionTo(target).opposite());
                        return true;
                    }
                }
            }
            for (int i = 0; i < 16; ++i) {
                for (int j = 0; j < myDirs.length; j++) {
                    if (myDirs[j] == dir) {
                        Location loc = myLoc.add(dir);
                        if (uc.canMove(dir) && ((!isEnemies && (enemyBase == null || (loc.distanceSquared(enemyBase) > baseRange) || (uc.canSenseLocation(enemyBase) && uc.isObstructed(loc, enemyBase)))) || reckless)) {
                            uc.move(dir);
                            return true;
                        }
                        break;
                    }
                }
                if (!rotate && myLoc.add(dir.rotateLeft()).distanceSquared(target) > myLoc.add(dir.rotateRight()).distanceSquared(target)) {
                    rotateRight = true;
                    rotate = true;
                }
                Location newLoc = myLoc.add(dir);
                if (uc.isOutOfMap(newLoc)) rotateRight = !rotateRight;
                    //If I could not go in that direction and it was not outside of the map, then this is the latest obstacle found
                else lastObstacleFound = myLoc.add(dir);
                if (rotateRight) dir = dir.rotateRight();
                else dir = dir.rotateLeft();
            }

            for (int j = 0; j < myDirs.length; j++) {
                if (myDirs[j] == dir) {
                    Location loc = myLoc.add(dir);
                    if (uc.canMove(dir) && ((!isEnemies && (enemyBase == null || (loc.distanceSquared(enemyBase) > baseRange) || (uc.canSenseLocation(enemyBase) && uc.isObstructed(loc, enemyBase)))) || reckless)) {
                        uc.move(dir);
                        return true;
                    }
                    break;
                }
            }
        }

        return false;
    }

    void resetPathfinding(){
        lastObstacleFound = null;
        minDistToEnemy = INF;
    }

    public void doMicro(boolean dash) {
        MicroInfo[] microInfo = new MicroInfo[9];
        enemies = uc.senseUnits(myTeam.getOpponent());
        enemies2 = uc.senseUnits(Team.NEUTRAL);
        Location myLoc = uc.getLocation();
        for (int i = 0; i < 9; i++) {
            if (!uc.canMove(myDirs[i]) && !dash) continue;
            Location loc = myLoc.add(myDirs[i]);
            TileType tt = uc.senseTileTypeAtLocation(loc);
            if ((!myLoc.isEqual(loc) && uc.senseUnitAtLocation(loc) != null) || tt == TileType.MOUNTAIN || tt == TileType.WATER) continue;
            Location target = myLoc.add(myDirs[i]);
            microInfo[i] = new MicroInfo(myLoc.add(myDirs[i]));

            for (int j = 0; j < enemies.length; j++) {
                Location enemyLoc = enemies[j].getLocation();
                if (uc.canSenseLocation(enemyLoc) && uc.canSenseLocation(target) && uc.isObstructed(enemyLoc, target)) continue;
                if (!uc.canSenseLocation(target)) continue;
                isEnemies = true;
                UnitInfo enemy = enemies[j];
                int distance = microInfo[i].loc.distanceSquared(enemy.getLocation());
                microInfo[i].updateSafe(distance, enemy);
            }

            for (int j = 0; j < enemies2.length; j++) {
                Location enemyLoc = enemies2[j].getLocation();
                if (uc.canSenseLocation(enemyLoc) && uc.canSenseLocation(target) && uc.isObstructed(enemyLoc, target)) continue;
                if (!uc.canSenseLocation(target)) continue;
                isEnemies = true;
                UnitInfo enemy = enemies2[j];
                int distance = microInfo[i].loc.distanceSquared(enemy.getLocation());
                microInfo[i].updateSafe(distance, enemy);
            }
        }

        int bestIndex = 8;

        for (int i = 8; i >= 0; i--) {
            if (microInfo[i] == null) continue;
            TileType tt = uc.senseTileTypeAtLocation(microInfo[i].loc);
            if ((!myLoc.isEqual(microInfo[i].loc) && uc.senseUnitAtLocation(microInfo[i].loc) != null) || tt == TileType.MOUNTAIN || tt == TileType.WATER) continue;
            if (!microInfo[bestIndex].isBetter(microInfo[i])) bestIndex = i;
        }

        if (isEnemies) {
            microDir = myDirs[bestIndex];
        } else microDir = null;
    }

    class MicroInfo {
        int damage;
        int minDistToTargetEnemy;
        int minDistToAlternativeTarget;
        int minDistToEnemy;
        int minDistToNeutral;
        int minDistToRealEnemy;
        int distToEnemyBase;
        Location loc;

        public MicroInfo(Location loc) {
            this.loc = loc;
            damage = 0;
            minDistToTargetEnemy = 100000;
            minDistToEnemy = 100000;
            minDistToAlternativeTarget = 100000;
            minDistToNeutral = 100000;
            minDistToRealEnemy = 100000;
            distToEnemyBase = 100000;
            if (enemyBase != null) distToEnemyBase = loc.distanceSquared(enemyBase);
        }

        void updateSafe(int distance, UnitInfo enemy) {
            UnitType enemyType = enemy.getType();
            Team enemyTeam = enemy.getTeam();

            if (enemyType == UnitType.BARBARIAN) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) damage += 58;
            } else if (enemyType == UnitType.RANGER) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE) && distance >= enemy.getStat(UnitStat.MIN_ATTACK_RANGE)) damage += 33;
            } else if (enemyType == UnitType.KNIGHT) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) damage += 36;
            } else if (enemyType == UnitType.BASE) {
                if (distance <= baseRange) damage += 58;
            } else if (enemyType == UnitType.EXPLORER) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) damage += 10;
            } else if (enemyType == UnitType.MAGE) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) damage += 108;
            } else if (enemyType == UnitType.ASSASSIN) {
                if (distance <= enemy.getStat(UnitStat.ATTACK_RANGE)) damage += 148;
            }

            if (distance < minDistToTargetEnemy) {
                if (enemyType == UnitType.RANGER || enemyType == UnitType.CLERIC || enemyType == UnitType.MAGE || enemyType == UnitType.EXPLORER) minDistToTargetEnemy = distance;
            }

            if (distance < minDistToAlternativeTarget) {
                if (enemyType == UnitType.ASSASSIN || enemyType == UnitType.KNIGHT) minDistToAlternativeTarget = distance;
            }

            if (distance < minDistToEnemy) minDistToEnemy = distance;

            if (enemyTeam == Team.NEUTRAL && distance < minDistToNeutral) minDistToNeutral = distance;
            if (enemyTeam == myTeam.getOpponent() && distance < minDistToRealEnemy) minDistToRealEnemy = distance;
        }

        boolean canAttack() {
            return myType.getStat(UnitStat.ATTACK_RANGE) >= this.minDistToTargetEnemy && this.minDistToTargetEnemy >= myType.getStat(UnitStat.MIN_ATTACK_RANGE);
        }

        boolean isBetter(MicroInfo m) {
            if (distToEnemyBase != 100000 && minDistToTargetEnemy != 100000) {
                if (distToEnemyBase <= 50 && m.distToEnemyBase <= 50) {
                    isFleeing = true;
                    return distToEnemyBase >= m.distToEnemyBase;
                }
                else if (distToEnemyBase > 242) {
                    isFleeing = false;
                    return minDistToTargetEnemy <= m.minDistToTargetEnemy;
                }
                else {
                    if (isFleeing) return minDistToTargetEnemy >= m.minDistToTargetEnemy;
                    else return minDistToTargetEnemy <= m.minDistToTargetEnemy;
                }
            }

            if (minDistToRealEnemy != 100000) {
                if (minDistToTargetEnemy == 100000 && m.minDistToTargetEnemy == 100000) {
                    if (minDistToAlternativeTarget == 100000 && m.minDistToAlternativeTarget == 100000) {
                        return minDistToEnemy >= m.minDistToEnemy;
                    }
                    if (canAttack()) {
                        if (!m.canAttack()) return true;
                        if (minDistToAlternativeTarget <= 2 && m.minDistToAlternativeTarget <= 2) return damage <= m.damage;
                    }
                    return minDistToAlternativeTarget <= m.minDistToAlternativeTarget;
                }

                if (canAttack()) {
                    if (!m.canAttack()) return true;
                    if (minDistToTargetEnemy <= 2 && m.minDistToTargetEnemy <= 2) return damage <= m.damage;
                }
                return minDistToTargetEnemy <= m.minDistToTargetEnemy;
            }
            return minDistToNeutral <= m.minDistToNeutral;
        }
    }
}