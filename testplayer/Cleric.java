package testplayer;

import aic2022.user.UnitController;
import aic2022.user.UnitInfo;

public class Cleric extends MyUnit {

    ClericPathfinder pathfinder;

    Cleric(UnitController uc){
        super(uc);

        this.pathfinder = new ClericPathfinder(uc);
    }

    void playRound(){
        genericBehaviour(true);
        UnitInfo[] allies = uc.senseUnits(myTeam);

        attack.tryHeal(allies);
        if (uc.canMove()) {
            tryMove(allies, false);
        }
        attack.tryHeal(uc.senseUnits(myTeam));

        uc.writeOnSharedArray(constants.CLERIC_ALIVE, 1);
        finish();
    }

    void tryMove(UnitInfo[] allies, boolean reckless) {
        if (uc.readOnSharedArray(constants.GO_TO_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, false, false);
        }

        int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
        if (position != 0) {
            enemyBase = helper.intToRealLocation(uc.readOnSharedArray(constants.ENEMY_BASE_POSITION));
        }

        pathfinder.getNextLocationTarget(allyBase, false, true);
    }
}
