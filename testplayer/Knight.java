package testplayer;

import aic2022.user.*;

public class Knight extends MyUnit {

    KnightPathfinder pathfinder;

    Knight(UnitController uc){
        super(uc);

        this.pathfinder = new KnightPathfinder(uc);
    }

    void playRound(){
        genericBehaviour(false);

        attack.genericTryAttack(enemies);
        attack.genericTryAttack(neutrals);

        tryLvlUp();
        tryUseSkill(enemies, neutrals);

        enemies = uc.senseUnits(opponent);
        neutrals = uc.senseUnits(Team.NEUTRAL);
        tryMove(enemies, neutrals, false);
        if (uc.getInfo().getLevel() >= 2) {
            enemies = uc.senseUnits(opponent);
            neutrals = uc.senseUnits(Team.NEUTRAL);
            tryMove(enemies, neutrals, false);
        }

        attack.genericTryAttack(uc.senseUnits(opponent));
        attack.genericTryAttack(uc.senseUnits(Team.NEUTRAL));
        attack.tryAttackShrine(uc.senseShrines());

        finish();
    }

    void tryLvlUp() {
        if (uc.readOnSharedArray(constants.ASSASSIN_ALIVE) < 2) return;
        if (roundLastEnemySeen + 5 > round) return;
        if (uc.getInfo().getHealth() < 4/5 * uc.getInfo().getStat(UnitStat.MAX_HEALTH)) return;

        if (uc.getInfo().getLevel() == 1 && uc.readOnSharedArray(constants.RANGERS_LVL2) == 1) {
            if (round < 1250 && uc.canLevelUp()) {
                uc.levelUp();
            }
        }
    }

    void tryUseSkill(UnitInfo[] enemies, UnitInfo[] neutrals) {
        Location bestTarget = null;
        Location myLoc = uc.getLocation();

        for (UnitInfo enemy : enemies) {
            Location enemyLoc = enemy.getLocation();
            UnitType enemyType = enemy.getType();

            if (enemyType == UnitType.RANGER && myLoc.distanceSquared(enemyLoc) > 2) {
                if (uc.canUseSecondAbility(enemyLoc)) uc.useSecondAbility(enemyLoc);
                break;
            } else if (bestTarget == null && uc.canUseSecondAbility(enemyLoc)) bestTarget = enemyLoc;
        }

        if (bestTarget != null) {
            uc.useSecondAbility(bestTarget);
            return;
        }

        for (UnitInfo enemy : neutrals) {
            Location enemyLoc = enemy.getLocation();
            UnitType enemyType = enemy.getType();

            if (enemyType == UnitType.RANGER && myLoc.distanceSquared(enemyLoc) > 2) {
                if (uc.canUseSecondAbility(enemyLoc)) uc.useSecondAbility(enemyLoc);
                break;
            } else if (bestTarget == null && uc.canUseSecondAbility(enemyLoc)) bestTarget = enemyLoc;
        }

        if (bestTarget != null) {
            uc.useSecondAbility(bestTarget);
            return;
        }
    }

    void tryMove(UnitInfo[] enemies, UnitInfo[] neutrals, boolean reckless) {
        if (uc.readOnSharedArray(constants.DESTROY_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, true, false);
        } else if (uc.readOnSharedArray(constants.GO_TO_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, false, false);
        }

        Location globalShrine = manager.getClosestShrine();
        if (globalShrine != null)  {
            if (uc.canSenseLocation(globalShrine)) globalShrine = null;
        }

        int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
        if (position != 0) {
            enemyBase = helper.intToRealLocation(uc.readOnSharedArray(constants.ENEMY_BASE_POSITION));
        }

        if (neutrals.length > 0 && !inDungeon && (closestChest == null || !canSurviveReachChest(closestChest.getLocation()))) {
            move.kiteToBase(neutrals);
        }

        if (closestChest != null) {
            if (canSurviveReachChest(closestChest.getLocation())) {
                pathfinder.getNextLocationTarget(closestChest.getLocation(), true, true);
                getChest();
            } else if (inDungeon) {
                mustExitDungeon = true;
                doDungeon();
                if (inDungeon) pathfinder.getNextLocationTarget(closestDungeon, true, true);
            } else {
                doDefault(false);
            }
            resetExplore = true;
        }
        else if (closestShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(closestShrine.getLocation(), reckless, false);
            resetExplore = true;
        }
        else if (globalShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(globalShrine, reckless, false);
            resetExplore = true;
        }
        else if (globalEnemy != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(globalEnemy, reckless, false);
            resetExplore = true;
        }
        else if (obstructedChest != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(obstructedChest.getLocation(), false, true);
            getChest();
            resetExplore = true;
        }
        else if (closestDungeon != null) {
            if (!inDungeon && canEnterDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
                resetExplore = true;
            } else if (inDungeon && mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, true, true);
                resetExplore = true;
            } else {
                doDefault(reckless);
            }
        } else {
            doDefault(reckless);
        }

        doDungeon();
    }

    void doDefault(Boolean reckless) {
        pathfinder.getNextLocationTarget(move.explore(resetExplore), reckless, true);
        resetExplore = false;
    }

}
