package directplayer;

import aic2022.user.Location;
import aic2022.user.UnitController;

public class MemoryManager {

    private final UnitController uc;
    private final Constants constants = new Constants();
    private final Helper helper;

    // GENERAL

    MemoryManager(UnitController uc){
        this.uc = uc;
        this.helper = new Helper(uc);
    }

    public void resetCounter(int key) {
        for(int i = 0; i < constants.COUNTERS_SPACE; i++) {
            uc.writeOnSharedArray(key + i, 0);
        }
    }

    public void roundClearCounter(int key) {
        uc.writeOnSharedArray(key + (uc.getRound() + 1)%constants.COUNTERS_SPACE, 0);
    }

    public void increaseValue(int key, int amount) {
        this.roundClearCounter(key);
        int realId = key + uc.getRound()%constants.COUNTERS_SPACE;
        int value = uc.readOnSharedArray(realId);
        uc.writeOnSharedArray(realId, value + amount);
    }

    public void increaseValueByOne(int key) {
        this.increaseValue(key, 1);
    }

    public int readValue(int key) {
        this.roundClearCounter(key);
        int realId = key + (uc.getRound() - 1)%constants.COUNTERS_SPACE;
        int realIdThisRound = key + (uc.getRound())%constants.COUNTERS_SPACE;
        return Math.max(uc.readOnSharedArray(realId), uc.readOnSharedArray(realIdThisRound));
    }

    public int readValueThisRound(int key) {
        int realId = key + (uc.getRound())%constants.COUNTERS_SPACE;
        return uc.readOnSharedArray(realId);
    }

    // MAP FUNCTIONS

    public int getIndexMap(int locX, int locY) {
        return constants.ID_MAP_INFO + helper.locationToInt(locX, locY) * constants.INFO_PER_CELL;
    }

    public int getIndexMap(Location loc) {
        return constants.ID_MAP_INFO + helper.locationToInt(loc) * constants.INFO_PER_CELL;
    }

    public void setPortalSafe(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index, constants.SAFE_PORTAL);
    }

    public void setPortalDangerous(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index, constants.DANGEROUS_PORTAL);
    }

    public int getPortalState(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index);
    }

    public void setShrineNeutral(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 1, constants.NEUTRAL_SHRINE);
    }

    public void setShrineAllied(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 1, constants.ALLIED_SHRINE);
    }

    public void setShrineEnemy(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + 1, constants.ENEMY_SHRINE);
    }

    public int getShrineState(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index + 1);
    }

}