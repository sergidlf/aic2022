package directplayer;

public class Constants {

    // MEMORY CONSTANTS
    public int ID_MAP_INFO = 100000;
    public int BASE_POSITION = 1;
    public int ENEMY_BASE_POSITION = 2;
    public int MAP_EDGE_X1 = 3;
    public int MAP_EDGE_X2 = 4;
    public int MAP_EDGE_Y1 = 5;
    public int MAP_EDGE_Y2 = 6;



    public int COUNTERS_SPACE = 3;
    public int SAFE_PORTAL = 1;
    public int DANGEROUS_PORTAL = 2;
    public int NEUTRAL_SHRINE = 1;
    public int ALLIED_SHRINE = 2;
    public int ENEMY_SHRINE = 3;
    public int INFO_PER_CELL = 5;
}