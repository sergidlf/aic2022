package directplayer;

import aic2022.user.UnitController;
import aic2022.user.UnitType;

public class UnitPlayer {

	public void run(UnitController uc) {
		UnitType t = uc.getType();
		MyUnit u = null;

		if (t == UnitType.BASE) {
			u = new Base(uc);
		} else if (t == UnitType.RANGER) {
			u = new Ranger(uc);
		} else if (t == UnitType.BARBARIAN) {
			u = new Barbarian(uc);
		} else if (t == UnitType.KNIGHT) {
			u = new Knight(uc);
		}

		while (true) {
			u.playRound();
			uc.yield();
		}
	}
}

