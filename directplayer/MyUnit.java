package directplayer;

import aic2022.user.*;

public abstract class MyUnit {

    UnitController uc;
    Move move;
    Attack attack;
    Helper helper;
    Location enemyBase = null;
    Team myTeam;

    ChestInfo closestChest = null;
    ShrineInfo closestShrine = null;
    Location closestDungeon = null;
    int closestDungeonState;
    MemoryManager manager;
    public final Constants constants = new Constants();

    int roundsUntilReset = 0;
    int roundsInside = 0;
    int roundsOutside = 0;

    boolean inDungeon = false;
    boolean canEnterDungeon = false;
    boolean mustExitDungeon = false;

    MyUnit(UnitController uc) {
        this.uc = uc;
        this.move = new Move(uc);
        this.attack = new Attack(uc);
        this.helper = new Helper(uc);
        this.myTeam = uc.getTeam();
        this.manager = new MemoryManager(uc);
    }

    abstract void playRound();

    public void setOrientation() {
        UnitInfo[] enemies = uc.senseUnits(uc.getTeam().getOpponent());
        Location myLoc = uc.getLocation();

        for (int j = 0; j < enemies.length; j++) {
            Location enemyLoc = enemies[j].getLocation();
            if (uc.isObstructed(enemyLoc, myLoc)) continue;
            uc.setOrientation(myLoc.directionTo(enemyLoc));
            break;
        }
    }

    public void getChest() {
        Location chestLoc = closestChest.getLocation();
        if (uc.canOpenChest(uc.getLocation().directionTo(chestLoc))) {
            uc.openChest(uc.getLocation().directionTo(chestLoc));
            closestChest = null;
            ArtifactInfo[] artifacts = uc.getArtifacts();
            if (artifacts.length > 0) {
                if (uc.canUseArtifact(0)) {
                    uc.useArtifact(0);
                }
            }
        }
    }

    public void doDungeon() {
        if (inDungeon && mustExitDungeon) {
            if (uc.getLocation().distanceSquared(closestDungeon) <= 2) {

                Direction dirEnter = uc.getLocation().directionTo(closestDungeon);
                Direction[] myDirs = Direction.values();

                for (Direction dir: myDirs) {
                    if (uc.canEnterDungeon(dirEnter, dir)) {
                        uc.enterDungeon(dirEnter, dir);
                        inDungeon = false;
                        mustExitDungeon = false;
                        roundsInside = 0;
                        break;
                    }
                }
            }
        } else if (!inDungeon && canEnterDungeon) {
            if (uc.getLocation().distanceSquared(closestDungeon) <= 2) {                // Check enter dungeon
                Direction dirEnter = uc.getLocation().directionTo(closestDungeon);
                Direction[] myDirs = Direction.values();

                for (Direction dir : myDirs) {
                    if (uc.canEnterDungeon(dirEnter, dir)) {
                        uc.enterDungeon(dirEnter, dir);
                        inDungeon = true;
                        roundsOutside = 0;

                        if (closestDungeonState == 0) {
                            UnitInfo[] enemies = uc.senseUnits(uc.getTeam().getOpponent());
                            UnitInfo[] neutrals = uc.senseUnits(Team.NEUTRAL);

                            if (enemies.length + neutrals.length < 2) {
                                manager.setPortalSafe(closestDungeon);
                            } else {
                                manager.setPortalDangerous(closestDungeon);
                                mustExitDungeon = true;
                            }
                        }

                        break;
                    }
                }
            }
        }
    }

    public void genericBehaviour() {
        roundsUntilReset = GameConstants.DUNGEON_RESET_ROUNDS - uc.getRound() % GameConstants.DUNGEON_RESET_ROUNDS;

        if (inDungeon) roundsInside++;
        else roundsOutside++;

        if (roundsUntilReset < 150) mustExitDungeon = true;
        if (roundsUntilReset > 300) canEnterDungeon = true;
        else canEnterDungeon = false;

        // Sense chests
        ChestInfo[] chests = uc.senseChests();
        if (chests.length == 0) closestChest = null;
        for (int i = 0; i < chests.length; i++) {
            if (!move.isObstructed(chests[i].getLocation())) {
                closestChest = chests[i];
                break;
            }
        }

        // Sense shrines
        ShrineInfo[] shrines = uc.senseShrines();
        for (int i = 0; i < shrines.length; i++) {
            Location shrineLoc = shrines[i].getLocation();
            if (!move.isObstructed(shrineLoc)) {
                Team owner = shrines[i].getOwner();

                if (owner == myTeam) {
                    manager.setShrineAllied(shrineLoc);
                    if (closestShrine != null && closestShrine.getLocation().isEqual(shrines[i].getLocation())) closestShrine = null;
                } else if (owner == Team.NEUTRAL) {
                    manager.setShrineNeutral(shrineLoc);
                } else {
                    manager.setShrineEnemy(shrineLoc);
                }

                if (owner != myTeam) {
                    closestShrine = shrines[i];
                    break;
                }
            }
        }

        // Sense dungeon entrance
        Location[] dungeons = uc.senseVisibleTiles(TileType.DUNGEON_ENTRANCE);
        for (int i = 0; i < dungeons.length; i++) {
            if (!move.isObstructed(dungeons[i])) {
                closestDungeonState = manager.getPortalState(dungeons[i]);
                if (closestDungeonState != constants.DANGEROUS_PORTAL) {
                    closestDungeon = dungeons[i];
                    break;
                }
            }
        }
    }
}