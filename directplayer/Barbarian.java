package directplayer;

import aic2022.user.Team;
import aic2022.user.UnitController;
import aic2022.user.UnitInfo;

public class Barbarian extends MyUnit {

    int round = 0;
    BarbarianPathfinder pathfinder;

    Barbarian(UnitController uc){
        super(uc);

        this.pathfinder = new BarbarianPathfinder(uc);
    }

    void playRound(){
        round = uc.getRound();

        //genericBehaviour();

        UnitInfo[] enemies = uc.senseUnits(uc.getTeam().getOpponent());
        attack.genericTryAttack(enemies);
        UnitInfo[] neutrals = uc.senseUnits(Team.NEUTRAL);
        attack.genericTryAttack(neutrals);
        if (uc.canMove()) {
            tryMove(enemies, neutrals, false);
        }
        attack.genericTryAttack(uc.senseUnits(uc.getTeam().getOpponent()));
        attack.genericTryAttack(uc.senseUnits(Team.NEUTRAL));
        attack.tryAttackShrine(uc.senseShrines());

        setOrientation();
    }

    void tryMove(UnitInfo[] enemies, UnitInfo[] neutrals, boolean reckless) {
        int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
        if (position != 0) {
            enemyBase = helper.intToRealLocation(uc.readOnSharedArray(constants.ENEMY_BASE_POSITION));
        }

        if (closestChest != null) {
            pathfinder.getNextLocationTarget(closestChest.getLocation(), reckless, true);
            getChest();
        }
        else if (closestShrine != null) pathfinder.getNextLocationTarget(closestShrine.getLocation(), reckless, false);
        else if (closestDungeon != null) {
            if (!inDungeon && canEnterDungeon && !mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
            } else if (inDungeon && mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
            } else {
                doDefault(reckless);
            }
        } else {
            doDefault(reckless);
        }
    }

    void doDefault(Boolean reckless) {
        pathfinder.getNextLocationTarget(move.explore(), reckless, true);
    }
}
