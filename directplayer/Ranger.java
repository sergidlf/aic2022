package directplayer;

import aic2022.user.Team;
import aic2022.user.UnitController;

public class Ranger extends MyUnit {

    RangerPathfinder pathfinder;
    int round = 0;

    Ranger(UnitController uc){
        super(uc);

        this.pathfinder = new RangerPathfinder(uc);
    }

    void playRound(){
        round = uc.getRound();

        //genericBehaviour();

        attack.genericTryAttack(uc.senseUnits(uc.getTeam().getOpponent()));
        attack.genericTryAttack(uc.senseUnits(Team.NEUTRAL));
        if (uc.canMove()) {
            tryMove(false);
        }
        attack.genericTryAttack(uc.senseUnits(uc.getTeam().getOpponent()));
        attack.genericTryAttack(uc.senseUnits(Team.NEUTRAL));
        attack.tryAttackShrine(uc.senseShrines());

        setOrientation();
    }

    void tryMove(boolean reckless) {
        int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
        if (position != 0) {
            enemyBase = helper.intToRealLocation(uc.readOnSharedArray(constants.ENEMY_BASE_POSITION));
        }

        if (closestChest != null) {
            pathfinder.getNextLocationTarget(closestChest.getLocation(), reckless, true);
            getChest();
        }
        else if (closestShrine != null) pathfinder.getNextLocationTarget(closestShrine.getLocation(), reckless, false);
        else if (closestDungeon != null) {
            if (!inDungeon && canEnterDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
            } else if (inDungeon && mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
            } else {
                doDefault(reckless);
            }
        } else {
            doDefault(reckless);
        }
    }

    void doDefault(Boolean reckless) {
        if (enemyBase == null) pathfinder.getNextLocationTarget(move.explore(), reckless, true);
        else pathfinder.getNextLocationTarget(enemyBase, reckless, true);
    }

}
