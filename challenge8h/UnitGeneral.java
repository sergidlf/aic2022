package challenge8h;

import aic2022.user.*;

public class UnitGeneral {
    UnitController uc;
    Attack attack;
    Move move;
    UnitType myType;
    ChestInfo closestChest;
    ShrineInfo closestShrine;
    Location closestDungeon;

    int roundsUntilReset = 0;
    int roundsInside = 0;
    int roundsOutside = 0;

    boolean canEnter = false;

    boolean inDungeon = false;

    public UnitGeneral(UnitController uc) {
        this.uc = uc;
        this.move = new Move(uc);
        this.attack = new Attack(uc);
        this.myType = uc.getType();
    }

    public void genericBehaviour() {
        roundsUntilReset = GameConstants.DUNGEON_RESET_ROUNDS - uc.getRound() % GameConstants.DUNGEON_RESET_ROUNDS;
        if (inDungeon) {
            roundsInside++;
            roundsOutside = 0;
        } else {
            roundsOutside++;
            roundsInside = 0;
        }

        // Sense chests
        ChestInfo[] chests = uc.senseChests();
        int closestChestIndex = -1;
        for(int i=0; i<chests.length; i++) {
            if (!move.isObstructed(chests[i].getLocation())) {
                closestChest = chests[i];
                closestChestIndex = i;
                break;
            }
        }

        // Sense shrines
        ShrineInfo[] shrines = uc.senseShrines();
        int closestShrineIndex = -1;
        for(int i=0; i<shrines.length; i++) {
            if (!move.isObstructed(shrines[i].getLocation()) && shrines[i].getOwner() != uc.getTeam()) {
                closestShrine = shrines[i];
                closestShrineIndex = i;
                break;
            }
        }

        // Sense dungeon entrance
        Location[] dungeons = uc.senseVisibleTiles(TileType.DUNGEON_ENTRANCE);
        int closestDungeonIndex = -1;
        for(int i=0; i<dungeons.length; i++) {
            if (!move.isObstructed(dungeons[i])) {
                closestDungeon = dungeons[i];
                closestDungeonIndex = i;
                break;
            }
        }

        if (closestChestIndex > -1) {
            Location chestLoc = closestChest.getLocation();
            if (uc.getLocation().distanceSquared(chestLoc) > 2) {
                // Go to chest if we can
                move.moveTo(chestLoc, false);
                if (uc.canOpenChest(uc.getLocation().directionTo(chestLoc))) {
                    uc.openChest(uc.getLocation().directionTo(chestLoc));
                    ArtifactInfo[] artifacts = uc.getArtifacts();
                    if (artifacts.length > 0) {
                        if (uc.canUseArtifact(0)) {
                            uc.useArtifact(0);
                        }
                    }
                }
            }
        } else if (closestShrineIndex > -1) {
            Location shrineLoc = closestShrine.getLocation();
            if (uc.getLocation().distanceSquared(shrineLoc) > uc.getInfo().getStat(UnitStat.MIN_ATTACK_RANGE)+1) {
                // Go to shrine if we can
                if (!uc.getLocation().add(uc.getLocation().directionTo(shrineLoc)).isEqual(shrineLoc)) move.moveTo(shrineLoc, false);
            } else if (uc.getLocation().distanceSquared(shrineLoc) <= uc.getInfo().getStat(UnitStat.MIN_ATTACK_RANGE) ||
                    uc.getLocation().add(uc.getLocation().directionTo(shrineLoc)).isEqual(shrineLoc)) {
                // Get away from shrine if too close
                Direction[] tempDirs = Direction.values();
                for (Direction dir: tempDirs) {
                    if (dir == null) break;
                    if(uc.canMove(dir)) uc.move(dir);
                }
            }
            if(!uc.getLocation().isEqual(shrineLoc)) {
                uc.attack(shrineLoc);
            }
        } else if (closestDungeonIndex > -1) {
            // Go to portal or explore?
            if (inDungeon && (roundsUntilReset < 50 || roundsInside > 50)) {
                if (uc.getLocation().distanceSquared(closestDungeon) > 2) {
                    // Go to dungeon portal if we can
                    move.moveTo(closestDungeon, false);
                } else {
                    // Check enter dungeon
                    Direction dirEnter = uc.getLocation().directionTo(closestDungeon);
                    Direction[] myDirs = Direction.values();

                    for (Direction dir: myDirs) {
                        if (uc.canEnterDungeon(dirEnter, dir)) {
                            uc.enterDungeon(dirEnter, dir);
                            inDungeon = !inDungeon;
                            break;
                        }
                    }
                }
            } else if (!inDungeon && (roundsUntilReset > 100 || roundsOutside > 50)) {
                if (uc.getRound() % 50 == 0) {
                    UnitInfo[] allies = uc.senseUnits(35, uc.getTeam());
                    if (allies.length >= 4) canEnter = true;
                }

                if (uc.getLocation().distanceSquared(closestDungeon) > 2) {
                    // Go to dungeon portal if we can
                    move.moveTo(closestDungeon, false);
                } else {
                    // Check enter dungeon
                    if (canEnter && (uc.getRound() % 50 >= 1 && uc.getRound() % 50 < 7)) {
                        Direction dirEnter = uc.getLocation().directionTo(closestDungeon);
                        Direction[] myDirs = Direction.values();

                        for (Direction dir : myDirs) {
                            if (uc.canEnterDungeon(dirEnter, dir)) {
                                uc.enterDungeon(dirEnter, dir);
                                inDungeon = !inDungeon;
                                canEnter = false;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

}
