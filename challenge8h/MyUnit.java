package challenge8h;

import aic2022.user.UnitController;

public abstract class MyUnit {

    UnitController uc;
    UnitGeneral ug;
    Move move;
    Attack attack;
    Helper helper;

    public final Constants constants = new Constants();

    MyUnit(UnitController uc) {
        this.uc = uc;
        this.ug = new UnitGeneral(uc);
        this.move = new Move(uc);
        this.attack = new Attack(uc);
        this.helper = new Helper(uc);
    }

    abstract void playRound();
}