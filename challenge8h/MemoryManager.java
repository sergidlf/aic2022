package challenge8h;

import aic2022.user.Location;
import aic2022.user.UnitController;

public class MemoryManager {

    private final UnitController uc;
    private final Constants constants = new Constants();
    private final Helper helper;

    // GENERAL

    MemoryManager(UnitController uc){
        this.uc = uc;
        this.helper = new Helper(uc);
    }

    public void resetCounter(int key) {
        for(int i = 0; i < constants.COUNTERS_SPACE; i++) {
            uc.writeOnSharedArray(key + i, 0);
        }
    }

    public void roundClearCounter(int key) {
        uc.writeOnSharedArray(key + (uc.getRound() + 1)%constants.COUNTERS_SPACE, 0);
    }

    public void increaseValue(int key, int amount) {
        this.roundClearCounter(key);
        int realId = key + uc.getRound()%constants.COUNTERS_SPACE;
        int value = uc.readOnSharedArray(realId);
        uc.writeOnSharedArray(realId, value + amount);
    }

    public void increaseValueByOne(int key) {
        this.increaseValue(key, 1);
    }

    public int readValue(int key) {
        this.roundClearCounter(key);
        int realId = key + (uc.getRound() - 1)%constants.COUNTERS_SPACE;
        int realIdThisRound = key + (uc.getRound())%constants.COUNTERS_SPACE;
        return Math.max(uc.readOnSharedArray(realId), uc.readOnSharedArray(realIdThisRound));
    }

    public int readValueThisRound(int key) {
        int realId = key + (uc.getRound())%constants.COUNTERS_SPACE;
        return uc.readOnSharedArray(realId);
    }

    // MAP FUNCTIONS


    public int getIndexMap(int locX, int locY) {
        return constants.ID_MAP_INFO + helper.locationToInt(locX, locY) * constants.INFO_PER_CELL;
    }

    public int getIndexMap(Location loc) {
        return constants.ID_MAP_INFO + helper.locationToInt(loc) * constants.INFO_PER_CELL;
    }

}