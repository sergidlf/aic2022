package challenge8h;

import aic2022.user.*;

public class Base extends MyUnit {

    int round = 0;
    int roundSpawn = 0;
    int explorers = 0;
    Location baseLocation;
    Location enemyBase;
    Boolean isBaseClose = false;
    UnitType target;
    Direction[] safeSpawn = new Direction[8];

    Base(UnitController uc){
        super(uc);

        this.uc = uc;
        this.attack = new Attack(uc);
        this.helper = new Helper(uc);
    }

    void playRound(){
        round = uc.getRound();
        if (round == 0) init();

        if (round > 0) {
            attack.genericTryAttack(uc.senseUnits(uc.getTeam().getOpponent()));
            attack.genericTryAttack(uc.senseUnits(Team.NEUTRAL));
        }

        trySpawn();
    }

    void init() {
        baseLocation = uc.getLocation();
        uc.writeOnSharedArray(constants.BASE_POSITION_X, baseLocation.x);
        uc.writeOnSharedArray(constants.BASE_POSITION_Y, baseLocation.y);

        senseBase();
    }

    private void trySpawn(){
        double random;
        if (target == null) {
            random = uc.getRandomDouble();

            if (random < 0.2) target = UnitType.RANGER;
            else if (random < 0.7) target = UnitType.BARBARIAN;
            else target = UnitType.KNIGHT;
        }

        if (spawnSafe(target)) target = null;
    }

    private boolean spawnSafe(UnitType t){
        Direction[] myDirs = new Direction[8];
        int index = 0;

        for (Direction dir: safeSpawn) {
            if (uc.canSpawn(t, dir)) {
                myDirs[index] = dir;
                index++;
            }
        }

        if (myDirs[0] == null) return false;

        int random = (int)(uc.getRandomDouble()*index);

        uc.spawn(t, myDirs[random]);
        roundSpawn = round;
        return true;
    }

    void senseBase(){
        UnitInfo[] units = uc.senseUnits(uc.getTeam().getOpponent());
        for (UnitInfo unit: units) {
            if (unit.getType() == UnitType.BASE) {
                enemyBase = unit.getLocation();
                isBaseClose = true;
            }
        }

        if (!isBaseClose) {
            safeSpawn = Direction.values();
        } else {
            int index = 0;
            float range = UnitType.BASE.getStat(UnitStat.ATTACK_RANGE);
            Direction[] myDirs = Direction.values();
            Direction[] tempDirs = new Direction[9];
            for (Direction dir: myDirs) {
                Location target = baseLocation.add(dir);
                if (target.distanceSquared(enemyBase) > range || uc.isObstructed(target, enemyBase)) {
                    tempDirs[index] = dir;
                    index++;
                }
            }

            safeSpawn = new Direction[index];

            index = 0;
            for (Direction dir: tempDirs) {
                if (dir == null) break;
                safeSpawn[index] = dir;
                index++;
            }
        }
    }
}
