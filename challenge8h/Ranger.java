package challenge8h;

import aic2022.user.Location;
import aic2022.user.Team;
import aic2022.user.UnitController;

public class Ranger extends MyUnit {

    RangerPathfinder pathfinder;
    int round = 0;

    Ranger(UnitController uc){
        super(uc);

        this.pathfinder = new RangerPathfinder(uc);
    }

    void playRound(){
        round = uc.getRound();

        attack.genericTryAttack(uc.senseUnits(uc.getTeam().getOpponent()));
        attack.genericTryAttack(uc.senseUnits(Team.NEUTRAL));
        if (uc.canMove()) {
            tryMove(false);
        }
        attack.genericTryAttack(uc.senseUnits(uc.getTeam().getOpponent()));
        attack.genericTryAttack(uc.senseUnits(Team.NEUTRAL));
    }

    void tryMove(boolean reckless) {
        if (round < 400) return;
        Location enemyBase = null;
        int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
        if (position != 0) {
            enemyBase = helper.intToLocation(uc.readOnSharedArray(constants.ENEMY_BASE_POSITION));
        }

        pathfinder.getNextLocationTarget(move.explore(), reckless);
    }

}
