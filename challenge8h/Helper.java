package challenge8h;

import aic2022.user.Location;
import aic2022.user.UnitController;

public class Helper {
    UnitController uc;
    private final Constants constants = new Constants();

    Helper(UnitController uc){
        this.uc = uc;
    }

    public int locationToInt(int locX, int locY) {
        return (locX * 1100 + locY);
    }

    public int locationToInt(Location loc) {
        return locationToInt(loc.x, loc.y);
    }

    public Location intToLocation(int number) {
        return new Location((number / 1100), (number % 1100));
    }
}