package challenge8h;

public class Constants {

    // MEMORY CONSTANTS
    public int ID_MAP_INFO = 1000;
    public int BASE_POSITION_X = 1;
    public int BASE_POSITION_Y = 2;
    public int ENEMY_BASE_POSITION = 3;
    public int MAP_EDGE_X1 = 4;
    public int MAP_EDGE_X2 = 5;
    public int MAP_EDGE_Y1 = 6;
    public int MAP_EDGE_Y2 = 7;



    public int COUNTERS_SPACE = 3;
    public int INFO_PER_CELL = 5;
}