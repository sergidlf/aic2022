package spireplayer;

import aic2022.user.*;

public class Barbarian extends MyUnit {

    BarbarianPathfinder pathfinder;

    Barbarian(UnitController uc) {
        super(uc);

        this.pathfinder = new BarbarianPathfinder(uc);
    }

    void playRound() {
        genericBehaviour(false);

        attack.genericTryAttack(enemies);
        attack.genericTryAttack(neutrals);
        tryGiveArtifacts();
        if (uc.canMove()) {
            tryMove(enemies, neutrals, false);
        }

        attack.genericTryAttack(uc.senseUnits(opponent));
        attack.genericTryAttack(uc.senseUnits(Team.NEUTRAL));
        attack.tryAttackShrine(uc.senseShrines());
        
        tryGiveArtifacts();
        setOrientation();
    }

    void tryMove(UnitInfo[] enemies, UnitInfo[] neutrals, boolean reckless) {
        if (uc.readOnSharedArray(constants.DESTROY_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, true, false);
        } else if (uc.readOnSharedArray(constants.GO_TO_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, false, false);
        }

        Location globalShrine = manager.getClosestShrine();
        if (globalShrine != null && uc.canSenseLocation(globalShrine)) globalShrine = null;

        if (neutrals.length > 0 && !inDungeon && (closestChest == null || !canSurviveReachChest(closestChest.getLocation()))) {
            if (uc.getLocation().distanceSquared(allyBase) > 18) move.kiteToBase(neutrals);
        }

        if (closestChest != null) {
            pathfinder.getNextLocationTarget(closestChest.getLocation(), true, true);
            getChest();
            resetExplore = true;
        }
        else if (closestShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(closestShrine.getLocation(), reckless, false);
            resetExplore = true;
        }
        else if (globalShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(globalShrine, reckless, false);
            resetExplore = true;
        }
        else if (obstructedChest != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(obstructedChest.getLocation(), false, true);
            getChest();
            resetExplore = true;
        }
        else if (closestDungeon != null) {
            if (!inDungeon && canEnterDungeon && !mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
                resetExplore = true;
            } else if (inDungeon && mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, true, true);
                resetExplore = true;
            } else {
                doDefault(reckless);
            }
        } else {
            doDefault(reckless);
        }
    }

    void doDefault(boolean reckless) {
        pathfinder.getNextLocationTarget(move.explore(resetExplore), reckless, true);
        resetExplore = false;
    }}
