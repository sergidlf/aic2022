package spireplayer;

public class Constants {

    // COUNTERS
    public int RANGER_COUNTER = 1;
    public int EXPLORER_COUNTER = 4;
    public int BARBARIAN_COUNTER = 7;
    public int KNIGHT_COUNTER = 10;
    public int ASSASSIN_COUNTER = 13;
    public int CLERIC_COUNTER = 16;
    public int MAGE_COUNTER = 19;

    // FLAGS
    public int BASE_POSITION = 22;
    public int ENEMY_BASE_POSITION = 23;
    public int MAP_EDGE_X1 = 24;
    public int MAP_EDGE_X2 = 25;
    public int MAP_EDGE_Y1 = 26;
    public int MAP_EDGE_Y2 = 27;
    public int MAP_SIZE = 28;
    public int FOUND_ENEMY = 29;
    public int RANGERS_LVL2 = 30;
    public int KNIGHTS_LVL2 = 31;
    public int ASSASSIN_LVL3 = 32;
    public int NEUTRAL_UNITS = 33;
    public int HAS_WATER = 34;
    public int HAS_A_LOT_OF_WATER = 35;
    public int WATER_SHRINES = 36;
    public int HAS_SHRINES = 37;
    public int DESTROY_BASE = 38;
    public int GO_TO_BASE = 39;
    public int BASE_DANGER_LEVEL = 500;

    // MAP
    public int ID_MAP_INFO = 100000;
    public int SHRINE_LOCATION = 1000;
    public int DUNGEON_LOCATION = 2000;
    public int ASSASSIN_LIST = 3000;
    public int LAST_ENEMY_LOCATION = 4000;
    public int LAST_ROUND_ENEMY = 4001;

    // SPACES
    public int COUNTERS_SPACE = 3;
    public int INFO_PER_CELL = 5;
    public int ASSASSIN_INFO_PER_CELL = 6;
    public int SHRINE_MAX = 30;
    public int DUNGEON_MAX = 30;
    public int MAX_ASSASSIN = 10;

    // VALUES
    public int DUNGEON_CHESTS = 1;
    public int DUNGEON_CLEARED = 2;
    public int DUNGEON_CHEST_LOCATION = 3;
    public int DUNGEON_CHEST_LOCATION_NOT_PICKED = 4;
    public int SAFE_PORTAL = 1;
    public int DANGEROUS_PORTAL = 2;
    public int SHRINE_TEAM = 1;
    public int SHRINE_SAFETY = 2;
    public int SHRINE_LAST_SEEN = 3;
    public int SAFE_SHRINE = 1;
    public int DANGEROUS_SHRINE = 2;
    public int NEUTRAL_SHRINE = 1;
    public int ALLIED_SHRINE = 2;
    public int ENEMY_SHRINE = 3;
    public int BASE_SAFE = 1;
    public int BASE_ATTACKED = 2;
    public int BASE_HEAVILY_ATTACKED = 3;
    public int OBSTRUCTED_CHEST = 1;
    public int ASSASSIN_LOCATION = 0;
    public int ASSASSIN_ROUND = 1;
    public int ASSASSIN_ROOT_LEFT = 2;
    public int ASSASSIN_HP = 3;
    public int ASSASSIN_ID = 4;
    public int ASSASSIN_DEFENSE = 5;
}