package spireplayer;

import aic2022.user.*;

public class MemoryManager {

    UnitController uc;
    public final Constants constants = new Constants();
    Helper helper;

    // GENERAL

    MemoryManager(UnitController uc) {
        this.uc = uc;
        this.helper = new Helper(uc);
    }

    // COUNTERS

    public void roundClearCounters() {
        roundClearCounter(constants.BARBARIAN_COUNTER);
        roundClearCounter(constants.RANGER_COUNTER);
        roundClearCounter(constants.KNIGHT_COUNTER);
        roundClearCounter(constants.ASSASSIN_COUNTER);
        roundClearCounter(constants.MAGE_COUNTER);
        roundClearCounter(constants.CLERIC_COUNTER);
        roundClearCounter(constants.EXPLORER_COUNTER);
    }

    public void roundClearCounter(int key) {
        uc.writeOnSharedArray(key + (uc.getRound() + 1) % constants.COUNTERS_SPACE, 0);
    }

    public void increaseValue(int key, int amount) {
        int realId = key + uc.getRound() % constants.COUNTERS_SPACE;
        int value = uc.readOnSharedArray(realId);
        uc.writeOnSharedArray(realId, value + amount);
    }

    public void increaseValueByOne(int key) {
        this.increaseValue(key, 1);
    }

    public int readValue(int key) {
        int realId = key + (uc.getRound() - 1) % constants.COUNTERS_SPACE;
        int realIdThisRound = key + (uc.getRound()) % constants.COUNTERS_SPACE;
        return Math.max(uc.readOnSharedArray(realId), uc.readOnSharedArray(realIdThisRound));
    }

    public void countUnit(UnitType type) {
        if (type == UnitType.BARBARIAN) {
            increaseValueByOne(constants.BARBARIAN_COUNTER);
        } else if (type == UnitType.RANGER) {
            increaseValueByOne(constants.RANGER_COUNTER);
        } else if (type == UnitType.KNIGHT) {
            increaseValueByOne(constants.KNIGHT_COUNTER);
        } else if (type == UnitType.ASSASSIN) {
            increaseValueByOne(constants.ASSASSIN_COUNTER);
        } else if (type == UnitType.MAGE) {
            increaseValueByOne(constants.MAGE_COUNTER);
        } else if (type == UnitType.CLERIC) {
            increaseValueByOne(constants.CLERIC_COUNTER);
        } else if (type == UnitType.EXPLORER) {
            increaseValueByOne(constants.EXPLORER_COUNTER);
        }
    }

    // MAP FUNCTIONS

    public int getIndexMap(int locX, int locY) {
        return constants.ID_MAP_INFO + helper.locationToInt(locX, locY) * constants.INFO_PER_CELL;
    }

    public int getIndexMap(Location loc) {
        return constants.ID_MAP_INFO + helper.locationToInt(loc) * constants.INFO_PER_CELL;
    }

    public Location getAllyBase() {
        int position = uc.readOnSharedArray(constants.BASE_POSITION);
        return helper.intToRealLocation(position);
    }

    public void setPortalSafe(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index, constants.SAFE_PORTAL);
    }

    public void setPortalDangerous(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index, constants.DANGEROUS_PORTAL);
    }

    public int getPortalState(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index);
    }

    public void addChestDungeon(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + constants.DUNGEON_CHESTS, uc.readOnSharedArray(index + constants.DUNGEON_CHESTS) + 1);
    }

    public int getChestsDungeon(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index + constants.DUNGEON_CHESTS);
    }

    public void setChestDungeon(Location dungeonLoc, Location chestLoc) {
        int index = getIndexMap(dungeonLoc);
        uc.writeOnSharedArray(index + constants.DUNGEON_CHEST_LOCATION, helper.realLocationToInt(chestLoc.x, chestLoc.y));
    }

    public Location getChestDungeon(Location loc) {
        Location chestLoc = null;
        int index = getIndexMap(loc);
        int position = uc.readOnSharedArray(index + constants.DUNGEON_CHEST_LOCATION);
        if (position != 0) chestLoc = helper.intToRealLocation(position);
        return chestLoc;
    }

    public void setChestDungeonNotPicked(Location dungeonLoc, Location chestLoc) {
        int index = getIndexMap(dungeonLoc);
        uc.writeOnSharedArray(index + constants.DUNGEON_CHEST_LOCATION_NOT_PICKED, helper.realLocationToInt(chestLoc.x, chestLoc.y));
    }

    public Location getChestDungeonNotPicked(Location loc) {
        Location chestLoc = null;
        int index = getIndexMap(loc);
        int position = uc.readOnSharedArray(index + constants.DUNGEON_CHEST_LOCATION_NOT_PICKED);
        if (position != 0) chestLoc = helper.intToRealLocation(position);
        return chestLoc;
    }

    public void setDungeonCleared(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + constants.DUNGEON_CLEARED, 1);
    }

    public void resetDungeon(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + constants.DUNGEON_CLEARED, 0);
    }

    public boolean isDungeonCleared(Location loc) {
        int index = getIndexMap(loc);
        if (uc.readOnSharedArray(index + constants.DUNGEON_CLEARED) == 1) return true;
        return false;
    }

    public void resetDungeons() {
        for (int i = constants.DUNGEON_LOCATION; i < constants.DUNGEON_LOCATION + constants.DUNGEON_MAX; i++) {
            int position = uc.readOnSharedArray(i);

            if (position != 0) {
                Location dungeonLoc = helper.intToRealLocation(uc.readOnSharedArray(i));
                resetDungeon(dungeonLoc);
            }
        }
    }

    public Location getClosestSafeDungeon() {
        int bestChests = 0;
        Location bestLoc = null;

        for (int i = constants.DUNGEON_LOCATION; i < constants.DUNGEON_LOCATION + constants.DUNGEON_MAX; i++) {
            int position = uc.readOnSharedArray(i);

            if (position != 0) {
                Location dungeonLoc = helper.intToRealLocation(uc.readOnSharedArray(i));
                int state = getPortalState(dungeonLoc);
                int chests = getChestsDungeon(dungeonLoc);
                boolean isCleared = isDungeonCleared(dungeonLoc);
                if (state == 1 && chests > 0 && !isCleared) {
                    if (bestLoc == null || bestChests < chests) bestLoc = dungeonLoc;
                }
            }
        }

        return bestLoc;
    }

    public boolean safeDungeons() {
        for (int i = constants.DUNGEON_LOCATION; i < constants.DUNGEON_LOCATION + constants.DUNGEON_MAX; i++) {
            int position = uc.readOnSharedArray(i);

            if (position != 0) {
                Location dungeonLoc = helper.intToRealLocation(uc.readOnSharedArray(i));
                int state = getPortalState(dungeonLoc);
                if (state == 1) return true;
            }
        }
        return false;
    }

    public void setBaseDanger(int dangerLevel) {
        uc.writeOnSharedArray(constants.BASE_DANGER_LEVEL, dangerLevel);
    }

    public int getBaseDanger() {
        return uc.readOnSharedArray(constants.BASE_DANGER_LEVEL);
    }

    public void setShrineNeutral(Location loc) {
        Location myLoc = uc.getLocation();

        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + constants.SHRINE_TEAM, constants.NEUTRAL_SHRINE);

        int counter = 0;
        UnitInfo[] enemies = uc.senseUnits(Team.NEUTRAL);
        for (UnitInfo enemy : enemies) {
            if (!uc.isObstructed(myLoc, enemy.getLocation())) counter++;
        }

        addShrine(loc);
        if (counter < 2) setShrineSafe(loc);
        else setShrineDangerous(loc);
    }

    public void setShrineAllied(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + constants.SHRINE_TEAM, constants.ALLIED_SHRINE);
        setShrineLastSeen(loc);
    }

    public void setShrineEnemy(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + constants.SHRINE_TEAM, constants.ENEMY_SHRINE);
        addShrine(loc);
    }

    public void setShrineSafe(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + constants.SHRINE_SAFETY, constants.SAFE_SHRINE);
    }

    public void setShrineDangerous(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + constants.SHRINE_SAFETY, constants.DANGEROUS_SHRINE);
    }

    public void setShrineLastSeen(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index + constants.SHRINE_LAST_SEEN, uc.getRound());
    }

    public int getShrineLastSeen(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index + constants.SHRINE_LAST_SEEN);
    }

    public int getShrineState(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index + constants.SHRINE_TEAM);
    }

    public int getShrineSafety(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index + constants.SHRINE_SAFETY);
    }

    public void setObstructedChest(Location loc) {
        int index = getIndexMap(loc);
        uc.writeOnSharedArray(index, constants.OBSTRUCTED_CHEST);
    }

    public int getObstructedChest(Location loc) {
        int index = getIndexMap(loc);
        return uc.readOnSharedArray(index);
    }

    public void deleteAssassin(int index) {
        uc.writeOnSharedArray(index, 0);
        uc.writeOnSharedArray(index + constants.ASSASSIN_ID, 0);
        uc.writeOnSharedArray(index + constants.ASSASSIN_HP, 0);
        uc.writeOnSharedArray(index + constants.ASSASSIN_DEFENSE, 0);
        uc.writeOnSharedArray(index + constants.ASSASSIN_ROUND, 0);
        uc.writeOnSharedArray(index + constants.ASSASSIN_ROOT_LEFT, 0);
    }

    public void addAssassin(UnitInfo assassin) {
        Location loc = assassin.getLocation();
        int id = assassin.getID();
        int position = helper.realLocationToInt(loc.x, loc.y);
        int index = -1;
        int emptyIndex = -1;

        for (int i = constants.ASSASSIN_LIST; i < constants.ASSASSIN_LIST + (constants.ASSASSIN_INFO_PER_CELL * constants.MAX_ASSASSIN); i += constants.ASSASSIN_INFO_PER_CELL) {
            int read = uc.readOnSharedArray(i + constants.ASSASSIN_ID);
            if (read == id) {
                index = i;
                break;
            }
            if (read == 0 && emptyIndex == -1) {
                emptyIndex = i;
            }
        }

        if (index == -1 && emptyIndex != -1) {
            uc.writeOnSharedArray(emptyIndex, position);
            uc.writeOnSharedArray(emptyIndex + constants.ASSASSIN_ID, id);
            uc.writeOnSharedArray(emptyIndex + constants.ASSASSIN_HP, assassin.getHealth());
            uc.writeOnSharedArray(emptyIndex + constants.ASSASSIN_ROUND, uc.getRound());
            int root = (int) assassin.getCurrentMovementCooldown() - 1;
            uc.writeOnSharedArray(emptyIndex + constants.ASSASSIN_ROOT_LEFT, root);
            uc.writeOnSharedArray(emptyIndex + constants.ASSASSIN_DEFENSE, (int) assassin.getStat(UnitStat.DEFENSE));
        } else if (index != -1) {
            uc.writeOnSharedArray(index, position);
            uc.writeOnSharedArray(index + constants.ASSASSIN_HP, id);
            uc.writeOnSharedArray(index + constants.ASSASSIN_ROUND, uc.getRound());
            int root = (int) assassin.getCurrentMovementCooldown() - 1;
            uc.writeOnSharedArray(index + constants.ASSASSIN_ROOT_LEFT, root);
            uc.writeOnSharedArray(index + constants.ASSASSIN_DEFENSE, (int) assassin.getStat(UnitStat.DEFENSE));
        }
    }

    public void addShrine(Location loc) {
        int position = helper.realLocationToInt(loc.x, loc.y);
        boolean found = false;
        int emptyIndex = -1;

        for (int i = constants.SHRINE_LOCATION; i < constants.SHRINE_LOCATION + constants.SHRINE_MAX; i++) {
            int read = uc.readOnSharedArray(i);
            if (read == position) found = true;
            if (read == 0 && emptyIndex == -1) {
                emptyIndex = i;
            }
        }

        if (!found && emptyIndex != -1) {
            uc.writeOnSharedArray(emptyIndex, position);
            uc.writeOnSharedArray(constants.HAS_SHRINES, 1);
        }
    }

    public Location getClosestShrine() {
        Location myLoc = uc.getLocation();
        int minDistance = 10000000;
        Location closestShrine = null;
        for (int i = constants.SHRINE_LOCATION; i < constants.SHRINE_LOCATION + constants.SHRINE_MAX; i++) {
            int position = uc.readOnSharedArray(i);

            if (position != 0) {
                Location shrineLoc = helper.intToRealLocation(uc.readOnSharedArray(i));
                int state = getShrineState(shrineLoc);

                if (state == constants.ALLIED_SHRINE && getShrineLastSeen(shrineLoc) + 75 > uc.getRound()) continue;

                int distance = myLoc.distanceSquared(shrineLoc);

                if (distance < minDistance) {
                    minDistance = distance;
                    closestShrine = shrineLoc;
                }
            }
        }

        return closestShrine;
    }

    public void addDungeon(Location loc) {
        int position = helper.realLocationToInt(loc.x, loc.y);
        boolean found = false;
        int emptyIndex = -1;

        for (int i = constants.DUNGEON_LOCATION; i < constants.DUNGEON_LOCATION + constants.DUNGEON_MAX; i++) {
            int read = uc.readOnSharedArray(i);
            if (read == position) found = true;
            if (read == 0 && emptyIndex == -1) {
                emptyIndex = i;
            }
        }

        if (!found && emptyIndex != -1) {
            uc.writeOnSharedArray(emptyIndex, position);
        }
    }

    public boolean getRangerFlag() {
        if (uc.readOnSharedArray(constants.RANGERS_LVL2) == 1) return true;
        return false;
    }

    public boolean getKnightFlag() {
        if (uc.readOnSharedArray(constants.KNIGHTS_LVL2) == 1) return true;
        return false;
    }
}