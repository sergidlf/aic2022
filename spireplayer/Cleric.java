package spireplayer;

import aic2022.user.*;

public class Cleric extends MyUnit {

    ClericPathfinder pathfinder;

    Cleric(UnitController uc) {
        super(uc);

        this.pathfinder = new ClericPathfinder(uc);
    }

    void playRound() {
        genericBehaviour(true);

        UnitInfo[] allies = uc.senseUnits(myTeam);
        attack.tryHeal(allies);
        tryGiveArtifacts();
        if (uc.canMove()) tryMove(allies, false);
        attack.tryHeal(uc.senseUnits(myTeam));

        tryGiveArtifacts();
        setOrientation();
    }

    void tryMove(UnitInfo[] allies, boolean reckless) {
        if (uc.readOnSharedArray(constants.GO_TO_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, false, false);
        }

        pathfinder.getNextLocationTarget(allyBase, false, true);
    }
}
