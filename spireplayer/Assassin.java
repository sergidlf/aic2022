package spireplayer;

import aic2022.user.*;

public class Assassin extends MyUnit {

    AssassinPathfinder pathfinder;
    int moveCounter = 0;

    Assassin(UnitController uc) {
        super(uc);

        this.pathfinder = new AssassinPathfinder(uc);
    }

    void playRound() {
        genericBehaviour(true);

        tryLvlUp(enemies, neutrals);
        attack.assassinAttack(enemies, neutrals);
        if (uc.readOnSharedArray(constants.DESTROY_BASE) == 1) attack.assassinAttackBase(enemyBase);
        tryGiveArtifacts();
        tryMove(false, enemies, neutrals);
        tryUnstuck();
        attack.tryAttackShrineAssassin(uc.senseShrines(), uc.senseUnits(opponent), uc.senseUnits(Team.NEUTRAL));
        tryGiveArtifacts();
        setOrientation();
    }

    void tryUnstuck() {
        if (uc.canMove()) moveCounter++;
        if (moveCounter > 50 && enemies.length == 0) {
            if (pathfinder.tryFlee()) moveCounter = 0;
        }
    }

    void tryLvlUp(UnitInfo[] enemies, UnitInfo[] neutrals) {
        while (uc.canLevelUp() && ((enemies.length == 0 && neutrals.length == 0) || uc.getInfo().getLevel() < 3)) {
            uc.levelUp();
        }
    }

    void tryMove(boolean reckless, UnitInfo[] enemies, UnitInfo[] neutrals) {
        if (uc.readOnSharedArray(constants.DESTROY_BASE) == 1) {
            if (uc.getLocation().distanceSquared(enemyBase) > 8) pathfinder.getNextLocationTarget(enemyBase, true, false);
        } else if (uc.readOnSharedArray(constants.GO_TO_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, false, false);
        }

        Location globalShrine = manager.getClosestShrine();
        if (globalShrine != null)  {
            boolean isDangerous = manager.getShrineSafety(globalShrine) == constants.DANGEROUS_SHRINE;

            if (!isDangerous) noAttackRound = 0;
            if (uc.canSenseLocation(globalShrine)) globalShrine = null;
        }

        if (closestChest != null) {
            if (!tryShadowStep(closestChest.getLocation(), enemies, neutrals, true)) pathfinder.getNextLocationTarget(closestChest.getLocation(), reckless, true);
            getChest();
            resetExplore = true;
        }
        else if (closestShrine != null && !mustExitDungeon) {
            if (uc.senseUnits((int)uc.getInfo().getStat(UnitStat.VISION_RANGE), myTeam, true).length == 0) {
                tryShadowStep(closestShrine.getLocation(), enemies, neutrals, false);
            }
            pathfinder.getNextLocationTarget(closestShrine.getLocation(), reckless, false);
            resetExplore = true;
        }
        else if (globalShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(globalShrine, reckless, false);
            resetExplore = true;
        }
        else if (obstructedChest != null && !mustExitDungeon) {
            if (!tryShadowStep(obstructedChest.getLocation(), enemies, neutrals, true)) pathfinder.getNextLocationTarget(obstructedChest.getLocation(), reckless, true);
            getChest();
            resetExplore = true;
        }
        else if (closestDungeon != null) {
            if (!inDungeon && canEnterDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
                resetExplore = true;
            } else if (inDungeon && mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, true, true);
                resetExplore = true;
            }
        }
        else if (globalEnemy != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(globalEnemy, reckless, false);
            resetExplore = true;
        }

        doDefault(reckless);
    }

    void doDefault(boolean reckless) {
        pathfinder.getNextLocationTarget(move.explore(resetExplore), reckless, true);
        resetExplore = false;
    }

    boolean tryShadowStep(Location loc, UnitInfo[] enemies, UnitInfo[] neutrals, boolean canSelf) {
        if (!uc.canMove()) return false;

        Location myLoc = uc.getLocation();
        Direction[] dirs = Direction.values();

        for (Direction dir : dirs) {
            if (!canSelf && dir == Direction.ZERO) continue;
            Location target = loc.add(dir);
            if (uc.canUseFirstAbility(target)) {

                for (UnitInfo enemy : enemies) {
                    Location enemyLoc = enemy.getLocation();
                    int distance = target.distanceSquared(enemyLoc);
                    if (distance <= 2) return false;
                }

                for (UnitInfo enemy : neutrals) {
                    Location enemyLoc = enemy.getLocation();
                    int distance = target.distanceSquared(enemyLoc);
                    if (distance <= 2) return false;
                }

                if (uc.canMove(myLoc.directionTo(target)) && myLoc.distanceSquared(target) <= 2) return false;
                uc.useFirstAbility(target);
                return true;
            }
        }

        return false;
    }
}