package spireplayer;

import aic2022.user.*;

public class UnitPlayer {

	public void run(UnitController uc) {
		UnitType type = uc.getType();
		MyUnit unit = null;

		if (type == UnitType.BASE) {
			unit = new Base(uc);
		} else if (type == UnitType.RANGER) {
			unit = new Ranger(uc);
		} else if (type == UnitType.BARBARIAN) {
			unit = new Barbarian(uc);
		} else if (type == UnitType.KNIGHT) {
			unit = new Knight(uc);
		} else if (type == UnitType.EXPLORER) {
			unit = new Explorer(uc);
		} else if (type == UnitType.ASSASSIN) {
			unit = new Assassin(uc);
		} else if (type == UnitType.CLERIC) {
			unit = new Cleric(uc);
		}

		while (true) {
			unit.playRound();
			uc.yield();
		}
	}
}