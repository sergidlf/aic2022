package spireplayer;

import aic2022.engine.Unit;
import aic2022.user.*;

public abstract class MyUnit {

    UnitController uc;
    Move move;
    Attack attack;
    Helper helper;
    Team myTeam;
    Team opponent;
    UnitType myType;
    MemoryManager manager;
    public final Constants constants = new Constants();

    Location enemyBase = null;
    int noAttackRound = 200;
    ChestInfo closestChest = null;
    ChestInfo obstructedChest = null;
    int obstructedChestCounter = 0;
    ShrineInfo closestShrine = null;
    Location closestDungeon = null;
    Location farDungeon = null;
    int closestDungeonState;
    int round;
    int roundsUntilReset = 0;
    int roundsInside = 0;
    int roundsOutside = 0;
    int mapSize;

    Location allyBase;
    boolean inDungeon = false;
    Location dungeonEntrance = null;
    boolean canEnterDungeon = false;
    boolean mustExitDungeon = false;
    boolean resetExplore = true;
    int roundLastEnemySeen = 0;
    Location lastEnemySeen;
    UnitInfo[] enemies;
    UnitInfo[] neutrals;
    Location globalEnemy;

    MyUnit(UnitController uc) {
        this.uc = uc;
        this.move = new Move(uc);
        this.attack = new Attack(uc);
        this.helper = new Helper(uc);
        this.myTeam = uc.getTeam();
        this.opponent = myTeam.getOpponent();
        this.myType = uc.getType();
        this.manager = new MemoryManager(uc);
    }

    abstract void playRound();

    public void updateEnemyBase() {
        int position = uc.readOnSharedArray(constants.ENEMY_BASE_POSITION);
        if (position != 0) {
            enemyBase = helper.intToRealLocation(uc.readOnSharedArray(constants.ENEMY_BASE_POSITION));
            if (uc.canSenseLocation(enemyBase)) {
                UnitInfo base = uc.senseUnitAtLocation(enemyBase);
                if (base == null || base.getType() != UnitType.BASE || base.getTeam() != opponent) enemyBase = null;
            }
        }
        else enemyBase = null;
    }

    public void checkEnemyLevels(UnitInfo[] enemies) {
        Location myLoc = uc.getLocation();
        if (enemies.length != 0) {
            roundLastEnemySeen = round;
            lastEnemySeen = enemies[0].getLocation();
        }

        int position = uc.readOnSharedArray(constants.LAST_ENEMY_LOCATION);
        if (position != 0) globalEnemy = helper.intToRealLocation(position);
        else globalEnemy = null;

        for (UnitInfo enemy : enemies) {
            UnitType enemyType = enemy.getType();
            Location enemyLoc = enemy.getLocation();
            int level = enemy.getLevel();
            uc.writeOnSharedArray(constants.FOUND_ENEMY, 1);
            if (enemyType == UnitType.RANGER && level >= 2) {
                uc.writeOnSharedArray(constants.RANGERS_LVL2, 1);
            } else if (enemyType == UnitType.KNIGHT && level >= 2) {
                uc.writeOnSharedArray(constants.KNIGHTS_LVL2, 1);
            } else if (enemyType == UnitType.ASSASSIN && level >= 3) {
                uc.writeOnSharedArray(constants.ASSASSIN_LVL3, 1);
                if (myType == UnitType.RANGER && uc.canUseFirstAbility(enemyLoc)) uc.useFirstAbility(enemyLoc);
                else if (myType == UnitType.KNIGHT && uc.canUseSecondAbility(enemyLoc)) uc.useSecondAbility(enemyLoc);
                enemy = uc.senseUnitAtLocation(enemyLoc);
                if (enemy.getCurrentMovementCooldown() >= 2) manager.addAssassin(enemy);
            }

            if (globalEnemy == null) {
                if (uc.isObstructed(myLoc, enemyLoc)) continue;
                uc.writeOnSharedArray(constants.LAST_ENEMY_LOCATION, helper.realLocationToInt(enemyLoc.x, enemyLoc.y));
                uc.writeOnSharedArray(constants.LAST_ROUND_ENEMY, round);
                globalEnemy = enemyLoc;
            } else {
                if (uc.canSenseLocation(globalEnemy)) {
                    UnitInfo enemySensed = uc.senseUnitAtLocation(globalEnemy);
                    if (enemySensed == null || enemySensed.getTeam() != opponent || enemySensed.getType() == UnitType.BASE) {
                        uc.writeOnSharedArray(constants.LAST_ENEMY_LOCATION, 0);
                        globalEnemy = null;
                    }
                }
            }
        }
    }

    public void tryUseArtifacts() {
        ArtifactInfo[] artifacts = uc.getArtifacts();
        for (int i = 0; i < artifacts.length; i++) {
            if (uc.canUseArtifact(i) && checkArtifactStat(artifacts[i].getStat(), uc.getType())) uc.useArtifact(i);
        }
    }

    public void tryGiveArtifacts() {
        ArtifactInfo[] artifacts = uc.getArtifacts();
        UnitInfo[] allies = uc.senseUnits(2, myTeam);
        if (allies.length == 0) return;

        int myHealth = uc.getInfo().getHealth();
        boolean lowHealth = myHealth < 100;

        for (int i = 0; i < artifacts.length; i++) {
            UnitStat stat = artifacts[i].getStat();
            if (!lowHealth && checkArtifactStat(stat, myType)) continue;
            UnitInfo bestAlly = null;
            UnitInfo bestAllyLow = null;

            for (UnitInfo ally : allies) {
                UnitType allyType = ally.getType();

                if (!uc.canUseArtifact(i) || checkArtifactStat(stat, allyType)) {
                    if (bestAlly == null || bestAlly.getHealth() < ally.getHealth()) bestAlly = ally;
                } else if (lowHealth) {
                    if (bestAllyLow == null || bestAllyLow.getHealth() < ally.getHealth()) bestAllyLow = ally;
                }
            }

            if (bestAlly != null) {
                Direction dir = uc.getLocation().directionTo(bestAlly.getLocation());
                if (uc.canGiveArtifact(i, dir)) uc.giveArtifact(i, dir);
            } else if (bestAllyLow != null) {
                Direction dir = uc.getLocation().directionTo(bestAllyLow.getLocation());
                if (uc.canGiveArtifact(i, dir)) uc.giveArtifact(i, dir);
            }
        }
    }

    public boolean checkArtifactStat(UnitStat stat, UnitType type) {
        if (stat == UnitStat.ATTACK_RANGE && type == UnitType.RANGER) return true;
        if (stat == UnitStat.ATTACK && (type == UnitType.RANGER || type == UnitType.ASSASSIN)) return true;
        if (stat == UnitStat.DEFENSE && type == UnitType.KNIGHT) return true;
        if (stat == UnitStat.ATTACK_COOLDOWN && (type == UnitType.RANGER || type == UnitType.ASSASSIN)) return true;
        if (stat == UnitStat.ABILITY_1_RANGE && (type == UnitType.ASSASSIN || type == UnitType.KNIGHT)) return true;
        if (stat == UnitStat.ABILITY_2_RANGE && type == UnitType.KNIGHT) return true;
        if (stat == UnitStat.ABILITY_1_COOLDOWN && type == UnitType.RANGER) return true;
        if (stat == UnitStat.ABILITY_2_COOLDOWN && type == UnitType.KNIGHT) return true;
        if (stat == UnitStat.MOVEMENT_COOLDOWN && type == UnitType.RANGER) return true;
        if (stat == UnitStat.MAX_HEALTH && type == UnitType.KNIGHT) return true;

        return false;
    }

    public void setOrientation() {
        UnitInfo[] enemies = uc.senseUnits((int)uc.getInfo().getStat(UnitStat.VISION_RANGE), myTeam, true);
        Location myLoc = uc.getLocation();
        boolean orientationSet = false;

        for (UnitInfo enemy : enemies) {
            if (enemy.getCurrentAttackCooldown() >= 2) continue;
            Location enemyLoc = enemy.getLocation();
            if (move.worstObstruction(enemyLoc) == TileType.MOUNTAIN) continue;
            int dist = enemyLoc.distanceSquared(myLoc);
            boolean canMove = enemy.getCurrentMovementCooldown() < 2;
            if ((dist > enemy.getStat(UnitStat.ATTACK_RANGE) || dist < enemy.getStat(UnitStat.MIN_ATTACK_RANGE)) && !canMove) continue;
            uc.setOrientation(myLoc.directionTo(enemyLoc));
            orientationSet = true;
            break;
        }

        if (!orientationSet) {
            if (enemies.length != 0) uc.setOrientation(myLoc.directionTo(enemies[0].getLocation()));
            else if (lastEnemySeen != null && !lastEnemySeen.isEqual(myLoc)) uc.setOrientation(myLoc.directionTo(lastEnemySeen));
        }
    }

    public void getChest() {
        Direction[] dirs = Direction.values();

        for (Direction dir : dirs) {
            if (uc.canOpenChest(dir)) {
                uc.openChest(dir);
                closestChest = null;
                obstructedChest = null;
                obstructedChestCounter = 0;
                if (inDungeon) {
                    manager.addChestDungeon(dungeonEntrance);
                    manager.setDungeonCleared(dungeonEntrance);

                    if (manager.getChestDungeon(dungeonEntrance) == null) manager.setChestDungeon(dungeonEntrance, uc.getLocation().add(dir));
                }
            }
        }
    }

    public void doDungeon() {
        if (inDungeon && mustExitDungeon) {
            if (uc.getLocation().distanceSquared(closestDungeon) <= 2) {

                Direction dirEnter = uc.getLocation().directionTo(closestDungeon);
                Direction[] myDirs = Direction.values();

                for (Direction dir: myDirs) {
                    if (uc.canEnterDungeon(dirEnter, dir)) {
                        uc.enterDungeon(dirEnter, dir);
                        inDungeon = false;
                        closestDungeon = null;
                        closestChest = null;
                        dungeonEntrance = null;
                        mustExitDungeon = false;
                        roundsInside = 0;
                        break;
                    }
                }
            }
        } else if (!inDungeon && canEnterDungeon) {
            if (uc.getLocation().distanceSquared(closestDungeon) <= 2) {
                Direction dirEnter = uc.getLocation().directionTo(closestDungeon);
                Direction[] myDirs = Direction.values();

                for (Direction dir : myDirs) {
                    if (uc.canEnterDungeon(dirEnter, dir)) {
                        uc.enterDungeon(dirEnter, dir);
                        inDungeon = true;
                        roundsOutside = 0;
                        dungeonEntrance = closestDungeon;
                        manager.addDungeon(dungeonEntrance);

                        if (closestDungeonState == 0) {
                            UnitInfo[] enemies = uc.senseUnits(uc.getTeam().getOpponent());
                            UnitInfo[] neutrals = uc.senseUnits(Team.NEUTRAL);
                            int counter = 0;

                            for (UnitInfo enemy : enemies) {
                                if (enemy.getType() != UnitType.CLERIC) counter++;
                            }

                            for (UnitInfo enemy : neutrals) {
                                if (enemy.getType() != UnitType.CLERIC) counter++;
                            }

                            if (counter < 2) {
                                manager.setPortalSafe(closestDungeon);
                            } else {
                                manager.setPortalDangerous(closestDungeon);
                                mustExitDungeon = true;
                            }
                        }

                        closestDungeon = uc.senseVisibleTiles(TileType.DUNGEON_ENTRANCE)[0];

                        break;
                    }
                }
            }
        }
    }

    public boolean canSurviveReachChest(Location chestLoc) {
        int distance2Chest = uc.getLocation().distanceSquared(chestLoc);
        UnitInfo myInfo = uc.getInfo();
        Location myLoc = uc.getLocation();
        UnitInfo[] enemies = uc.senseUnits((int)uc.getType().getStat(UnitStat.VISION_RANGE), myTeam, true);

        int healthLost = 0;
        for(int i=0; i<enemies.length; i++) {
            if (enemies[i].getStat(UnitStat.ATTACK_RANGE) <= myLoc.distanceSquared(enemies[i].getLocation())) {
                healthLost += (enemies[i].getStat(UnitStat.ATTACK) - myInfo.getStat(UnitStat.DEFENSE)) / enemies[i].getStat(UnitStat.ATTACK_COOLDOWN);
            }
        }
        healthLost *= Math.sqrt(distance2Chest) - 2;
        if (myInfo.getHealth() <= healthLost) return false;

        int gold = uc.senseChestAtLocation(chestLoc).getGold();
        if (gold < myInfo.getStat(UnitStat.COST_GOLD) * healthLost/myInfo.getStat(UnitStat.MAX_HEALTH)) return false;

        return true;
    }

    void senseWaterShrines() {
        ShrineInfo[] shrines = uc.senseShrines();

        for (ShrineInfo shrine : shrines) {
            Team team = shrine.getOwner();
            Location shrineLoc = shrine.getLocation();
            TileType obstruction = move.worstObstruction(shrineLoc);
            int distance = allyBase.distanceSquared(shrineLoc);

            if (distance > uc.getInfo().getStat(UnitStat.ATTACK_RANGE) && obstruction == TileType.WATER && team != myTeam) {
                if (attack.couldAttackLocationBase(shrineLoc)) uc.writeOnSharedArray(constants.WATER_SHRINES, 1);
            }
        }
    }

    public void senseWater() {
        int water = uc.senseVisibleTiles(TileType.WATER).length;
        if (water > 20) uc.writeOnSharedArray(constants.HAS_WATER, 1);
        if (water > 45) uc.writeOnSharedArray(constants.HAS_A_LOT_OF_WATER, 1);
    }

    public void genericBehaviour(boolean extraCareful) {
        round = uc.getRound();
        allyBase = manager.getAllyBase();
        enemies = uc.senseUnits(opponent);
        neutrals = uc.senseUnits(Team.NEUTRAL);
        roundsUntilReset = GameConstants.DUNGEON_RESET_ROUNDS - round % GameConstants.DUNGEON_RESET_ROUNDS;
        farDungeon = manager.getClosestSafeDungeon();
        mapSize = uc.readOnSharedArray(constants.MAP_SIZE);
        int opponentRep = uc.getReputation(opponent);
        int rep = uc.getReputation();

        manager.countUnit(myType);
        updateEnemyBase();
        checkEnemyLevels(enemies);
        senseWaterShrines();
        senseWater();
        tryUseArtifacts();

        if (myType != UnitType.EXPLORER) {
            if (uc.getReputation() >= 50) noAttackRound = 0;
            if (noAttackRound != 0 && round < noAttackRound) {
                if (manager.safeDungeons()) noAttackRound = 0;
            }
        }

        if (uc.readOnSharedArray(constants.GO_TO_BASE) == 0) {
            if ((round > 1350 && enemyBase != null && rep <= opponentRep)) {
                uc.writeOnSharedArray(constants.GO_TO_BASE, 1);
            }
        } else {
            if (((uc.senseUnits(myTeam).length >= 9 || round > 1400) && uc.canSenseLocation(enemyBase))) {
                uc.writeOnSharedArray(constants.DESTROY_BASE, 1);
            }
        }

        if (obstructedChest != null) obstructedChestCounter++;
        if (obstructedChestCounter > 50) {
            manager.setObstructedChest(obstructedChest.getLocation());
            obstructedChest = null;
            obstructedChestCounter = 0;
        }

        ChestInfo[] chests = uc.senseChests();
        if (chests.length == 0) {
            closestChest = null;
            if (obstructedChest != null) {
                if (!uc.canSenseLocation(obstructedChest.getLocation())) {
                    manager.setObstructedChest(obstructedChest.getLocation());
                    obstructedChest = null;
                    obstructedChestCounter = 0;
                }
            }
        }
        for (ChestInfo chest : chests) {
            Location chestLoc = chest.getLocation();
            TileType obstruction = move.worstObstruction(chestLoc);
            if (obstruction == null) {
                closestChest = chest;
                break;
            } else {
                if (obstructedChest == null) {
                    if (manager.getObstructedChest(chestLoc) == 0 || (myType == UnitType.ASSASSIN && attack.couldAttackLocation(chestLoc)))
                    obstructedChest = chest;
                    obstructedChestCounter = 0;
                }
            }
        }

        if (inDungeon && manager.getChestDungeonNotPicked(dungeonEntrance) == null && closestChest != null) manager.setChestDungeonNotPicked(dungeonEntrance, closestChest.getLocation());

        ShrineInfo[] shrines = uc.senseShrines();
        for (ShrineInfo shrine : shrines) {
            Location shrineLoc = shrine.getLocation();
            Team owner = shrine.getOwner();

            if (owner == myTeam) {
                manager.setShrineAllied(shrineLoc);
                if (closestShrine != null && closestShrine.getLocation().isEqual(shrine.getLocation())) closestShrine = null;
            } else if (owner == Team.NEUTRAL) {
                manager.setShrineNeutral(shrineLoc);
            } else {
                manager.setShrineEnemy(shrineLoc);
            }

            if (attack.couldAttackLocation(shrineLoc)) {
                if (owner != myTeam) {
                    closestShrine = shrine;
                    break;
                }
            }
        }

        Location[] dungeons = uc.senseVisibleTiles(TileType.DUNGEON_ENTRANCE);
        int bestChest = 0;
        int currentState = 0;
        if (!inDungeon) closestDungeon = null;
        for (Location dungeon : dungeons) {
            if (!move.isObstructed(dungeon)) {
                currentState = manager.getPortalState(dungeon);
                int chestAmount = manager.getChestsDungeon(dungeon);
                boolean isCleared = manager.isDungeonCleared(dungeon);
                if (!inDungeon && (currentState != constants.DANGEROUS_PORTAL && (!extraCareful || currentState == constants.SAFE_PORTAL))) {
                    if (!isCleared && chestAmount > bestChest) {
                        bestChest = chestAmount;
                        closestDungeon = dungeon;
                        closestDungeonState = currentState;
                    }
                    else if (closestDungeon == null) {
                        closestDungeon = dungeon;
                        closestDungeonState = currentState;
                    }
                }
            }
        }

        if (inDungeon) {
            int enemyCounter = 0;
            for (UnitInfo enemy : neutrals) {
                UnitType enemyType = enemy.getType();
                if (enemyType != UnitType.CLERIC) enemyCounter++;
            }

            int chestAmount = manager.getChestsDungeon(dungeonEntrance) + uc.senseChests().length;
            if (enemyCounter > 1 && enemyCounter > chestAmount * 2) manager.setPortalDangerous(dungeonEntrance);
            roundsInside++;
        }
        else roundsOutside++;

        if (inDungeon) {
            boolean isCleared = manager.isDungeonCleared(dungeonEntrance) && roundsInside > 50;
            boolean isDangerous = manager.getPortalState(dungeonEntrance) == constants.DANGEROUS_PORTAL;
            Location chestLoc = manager.getChestDungeon(dungeonEntrance);
            boolean isStolen = !isCleared && chestLoc != null && uc.canSenseLocation(chestLoc) && uc.senseChestAtLocation(chestLoc) == null;
            Location chestLocNotPicked = manager.getChestDungeonNotPicked(dungeonEntrance);
            boolean alternativeStolen = !isCleared && chestLocNotPicked != null && uc.canSenseLocation(chestLocNotPicked) && uc.senseChestAtLocation(chestLocNotPicked) == null;

            if (isStolen || alternativeStolen) manager.setDungeonCleared(dungeonEntrance);
            if (isCleared || isDangerous || isStolen || alternativeStolen || roundsUntilReset < 25) mustExitDungeon = true;

            if (uc.senseChests().length == 0) {
                Location[] visibleLocations = uc.getVisibleLocations();
                boolean emptyDungeon = true;
                for (int i = visibleLocations.length - 1; i > visibleLocations.length - 9; i--) {
                    if (uc.senseTileTypeAtLocation(visibleLocations[i]) != TileType.MOUNTAIN) {
                        emptyDungeon = false;
                        break;
                    }
                }
                if (emptyDungeon) {
                    manager.setDungeonCleared(dungeonEntrance);
                    mustExitDungeon = true;
                }
            }
        }

        if (!inDungeon && closestDungeon != null && closestDungeonState != constants.DANGEROUS_PORTAL && !manager.isDungeonCleared(closestDungeon) && roundsUntilReset > 50) {
            canEnterDungeon = true;
            mustExitDungeon = false;
        }
        else canEnterDungeon = false;
    }
}