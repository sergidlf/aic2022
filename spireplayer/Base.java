package spireplayer;

import aic2022.user.*;

public class Base extends MyUnit {

    int round = 0;
    int roundSpawn = 0;
    int spawnCounter = 0;
    int assassinCounter = 0;
    Location enemyBase = null;
    Location lastEnemy = null;
    boolean isBaseClose = false;
    boolean woundedAlly = false;
    UnitType target;
    Direction[] safeSpawn = new Direction[8];

    Base(UnitController uc) {
        super(uc);

        this.uc = uc;
    }

    void playRound() {
        upkeep();

        UnitInfo[] enemies = uc.senseUnits(opponent);
        UnitInfo[] neutrals = uc.senseUnits(Team.NEUTRAL);
        UnitInfo[] allies = uc.senseUnits(myTeam);

        resetEnemyAssassins();
        checkWoundedAllies(allies);
        checkEnemyLevels(enemies);

        attack.genericTryAttack(enemies);
        attack.genericTryAttack(neutrals);
        attack.tryAttackShrine(uc.senseShrines());

        int dangerLevel = senseDanger(uc.senseUnits(opponent), uc.senseUnits(Team.NEUTRAL));
        trySpawn(dangerLevel);
        manager.roundClearCounters();
    }

    void upkeep() {
        updateBasePosition();
        updateEnemyBase();
        round = uc.getRound();
        if (round == 0) init();
        if (round % 400 == 0) manager.resetDungeons();
        int enemyPos = uc.readOnSharedArray(constants.LAST_ENEMY_LOCATION);
        if (enemyPos != 0) lastEnemy = helper.intToRealLocation(enemyPos);
        if (uc.readOnSharedArray(constants.LAST_ROUND_ENEMY) + 1 < round) uc.writeOnSharedArray(constants.LAST_ENEMY_LOCATION, 0);
    }

    void checkWoundedAllies(UnitInfo[] allies) {
        for (UnitInfo ally : allies) {
            if (ally.getHealth() < ally.getStat(UnitStat.MAX_HEALTH)) {
                woundedAlly = true;
                break;
            }
        }
    }

    void resetEnemyAssassins() {
        for (int i = constants.ASSASSIN_LIST; i < constants.ASSASSIN_LIST + (constants.ASSASSIN_INFO_PER_CELL * constants.MAX_ASSASSIN); i += constants.ASSASSIN_INFO_PER_CELL) {
            int root = uc.readOnSharedArray(i + constants.ASSASSIN_ROOT_LEFT);

                if (root == 1) {
                manager.deleteAssassin(i);
            } else if (root > 1) uc.writeOnSharedArray(i + constants.ASSASSIN_ROOT_LEFT, root - 1);
        }
    }

    void init() {
        updateBasePosition();
        setMapEdges();
        senseBase();
        senseWater();
    }

    void updateBasePosition() {
        allyBase = uc.getLocation();
        uc.writeOnSharedArray(constants.BASE_POSITION, helper.realLocationToInt(allyBase.x, allyBase.y));
    }

    void setMapEdges() {
        uc.writeOnSharedArray(constants.MAP_EDGE_X1, -1);
        uc.writeOnSharedArray(constants.MAP_EDGE_X2, 1100);
        uc.writeOnSharedArray(constants.MAP_EDGE_Y1, -1);
        uc.writeOnSharedArray(constants.MAP_EDGE_Y2, 1100);
    }

    void trySpawn(int dangerLevel) {
        assassinCounter = manager.readValue(constants.ASSASSIN_COUNTER);

        if (round == 0) target = UnitType.EXPLORER;
        if (uc.readOnSharedArray(constants.WATER_SHRINES) == 1) target = UnitType.RANGER;
        if (dangerLevel == constants.BASE_ATTACKED) target = UnitType.RANGER;
        if (uc.getReputation() >= 180 && assassinCounter < 2 && round < 1200) {
            target = UnitType.ASSASSIN;
        }
        if (target == UnitType.ASSASSIN && ((uc.getReputation() < 180 && assassinCounter == 0) || (uc.getReputation() < 240 && assassinCounter == 1) || round > 1200)) {
            target = null;
        }
        if (manager.readValue(constants.CLERIC_COUNTER) == 0 && dangerLevel == constants.BASE_SAFE && round < 1200 &&
                uc.getInfo().getHealth() < uc.getInfo().getStat(UnitStat.MAX_HEALTH) * 2 / 3) {
            target = UnitType.CLERIC;
        }
        if (dangerLevel == constants.BASE_HEAVILY_ATTACKED) target = UnitType.KNIGHT;

        if (target == null) {
            if (spawnCounter == 0 || spawnCounter == 2 || (spawnCounter == 3 && uc.readOnSharedArray(constants.HAS_WATER) == 1) || (spawnCounter == 1 && uc.readOnSharedArray(constants.HAS_A_LOT_OF_WATER) == 1)) {
                target = UnitType.RANGER;
            }
            else target = UnitType.KNIGHT;
        }

        if (spawnSafe(target, dangerLevel)) {
            manager.countUnit(target);
            if (target == UnitType.RANGER) uc.writeOnSharedArray(constants.WATER_SHRINES, 0);
            spawnCounter++;
            if (spawnCounter == 4) spawnCounter = 0;
            target = null;
        }
    }

    boolean spawnSafe(UnitType t, int dangerLevel) {
        Direction[] myDirs = new Direction[8];
        int index = 0;
        Location myLoc = uc.getLocation();

        for (Direction dir: safeSpawn) {
            if (uc.canSpawn(t, dir)) {
                myDirs[index] = dir;
                index++;
            }
        }

        if (myDirs[0] == null) return false;

        UnitInfo[] enemies = uc.senseUnits((int)UnitType.BASE.getStat(UnitStat.VISION_RANGE), myTeam, true);

        if (enemies.length == 0) {
            if (lastEnemySeen != null) {
                Direction spawnDir = allyBase.directionTo(lastEnemy).opposite();
                if (uc.canSpawn(t, spawnDir)) {
                    uc.spawn(t, spawnDir);
                    return true;
                }
            }

            int random = (int)(uc.getRandomDouble() * index);

            uc.spawn(t, myDirs[random]);
            roundSpawn = round;
            return true;
        } else {
            int lowestDamage = 10000;
            Direction bestDir = null;

            for (Direction dir : myDirs) {
                if (dir == null) continue;
                int damage = 0;
                for (UnitInfo enemy : enemies) {
                    Location loc = enemy.getLocation();
                    if (move.isObstructed(loc, myLoc.add(dir))) continue;
                    if (enemy.getStat(UnitStat.ATTACK_RANGE) >= loc.distanceSquared(myLoc.add(dir))) {
                        damage += enemy.getStat(UnitStat.ATTACK);
                    }
                }

                if (damage < lowestDamage) {
                    lowestDamage = damage;
                    bestDir = dir;
                }
            }

            if (bestDir != null && (lowestDamage <= 45 || dangerLevel == constants.BASE_HEAVILY_ATTACKED)) {
                uc.spawn(t, bestDir);
                roundSpawn = round;
                return true;
            }
        }

        return false;
    }

    int senseDanger(UnitInfo[] enemies, UnitInfo[] neutrals) {
        int enemyCounterIn = 0;
        int enemyCounterOut = 0;
        int dangerLevel;

        for (UnitInfo enemy : enemies) {
            Location enemyLoc = enemy.getLocation();
            boolean isObstructed = move.isObstructed(enemyLoc);
            if (allyBase.distanceSquared(enemyLoc) <= uc.getInfo().getStat(UnitStat.ATTACK_RANGE) && !isObstructed) enemyCounterIn++;
            else if (!isObstructed) enemyCounterOut++;
        }
        for (UnitInfo neutral : neutrals) {
            Location enemyLoc = neutral.getLocation();
            boolean isObstructed = move.isObstructed(enemyLoc);
            if (allyBase.distanceSquared(enemyLoc) <= uc.getInfo().getStat(UnitStat.ATTACK_RANGE) && !isObstructed) enemyCounterIn++;
            else if (!isObstructed) enemyCounterOut++;
        }

        if (enemyCounterIn > 0) {
            dangerLevel = constants.BASE_HEAVILY_ATTACKED;
        } else if (enemyCounterOut > 1) {
            dangerLevel = constants.BASE_ATTACKED;
        } else {
            dangerLevel = constants.BASE_SAFE;
        }

        manager.setBaseDanger(dangerLevel);
        return dangerLevel;
    }

    void senseBase() {
        UnitInfo[] units = uc.senseUnits(opponent);
        for (UnitInfo unit: units) {
            if (unit.getType() == UnitType.BASE) {
                enemyBase = unit.getLocation();
                uc.writeOnSharedArray(constants.ENEMY_BASE_POSITION, helper.realLocationToInt(enemyBase.x, enemyBase.y));
                isBaseClose = true;
            }
        }

        if (!isBaseClose) {
            safeSpawn = Direction.values();
        } else {
            int index = 0;
            float range = UnitType.BASE.getStat(UnitStat.ATTACK_RANGE);
            Direction[] myDirs = Direction.values();
            Direction[] tempDirs = new Direction[9];
            for (Direction dir: myDirs) {
                Location baseTarget = allyBase.add(dir);
                if (baseTarget.distanceSquared(enemyBase) > range || uc.isObstructed(baseTarget, enemyBase)) {
                    tempDirs[index] = dir;
                    index++;
                }
            }

            safeSpawn = new Direction[index];

            index = 0;
            for (Direction dir: tempDirs) {
                if (dir == null) break;
                safeSpawn[index] = dir;
                index++;
            }
        }
    }
}
