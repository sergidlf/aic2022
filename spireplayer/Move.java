package spireplayer;

import aic2022.user.*;
import java.util.function.Function;

public class Move {

    UnitController uc;
    public final Constants constants = new Constants();
    Helper helper;
    MemoryManager manager;

    int INF = 1000000000;
    int counter = 0;
    int x1 = -1;
    int x2 = 1100;
    int y1 = -1;
    int y2 = 1100;
    Location enemyBase = null;
    Location edgeTarget = null;
    boolean x1found = false;
    boolean x2found = false;
    boolean y1found = false;
    boolean y2found = false;
    boolean edgesFound = false;

    public Move(UnitController uc) {
        this.uc = uc;
        this.helper = new Helper(uc);
        this.manager = new MemoryManager(uc);
    }

    boolean safeLocation(Location loc, Location[] dangerLocs, boolean reckless) {
        if (reckless) return true;

        boolean isSafe = true;

        for(Location danger: dangerLocs) {
            if (loc.isEqual(danger)) {
                isSafe = false;
                break;
            }
        }

        return isSafe;
    }

    Location[] dangerousLocations() {
        Direction[] dirs = Direction.values();
        Location myLoc = uc.getLocation();
        Location[] dangerLocs = new Location[9];
        int index = 0;

        for (Direction dir: dirs) {
            Location target = myLoc.add(dir);

            if (enemyBase != null) {
                int dist = enemyBase.distanceSquared(target);
                if (dist <= UnitType.BASE.getStat(UnitStat.ATTACK_RANGE)) {
                    dangerLocs[index] = target;
                    index++;
                    continue;
                }
            }

            index++;
        }
        return dangerLocs;
    }

    boolean isObstructed(Location loc1, Location loc2) {
        int counter = 0;
        while (loc2.distanceSquared(loc1) > 0 && counter < 20) {
            loc2 = loc2.add(loc2.directionTo(loc1));
            TileType tt = uc.senseTileTypeAtLocation(loc2);
            if (tt.equals(TileType.MOUNTAIN)) return true;
            counter++;
        }
        if (counter >= 20) {
            return true;
        }
        return false;
    }

    boolean isObstructed(Location loc) {
        Location currentLoc = uc.getLocation();
        int counter = 0;
        while (currentLoc.distanceSquared(loc) > 0 && counter < 20) {
            currentLoc = currentLoc.add(currentLoc.directionTo(loc));
            TileType tt = uc.senseTileTypeAtLocation(currentLoc);
            if (tt.equals(TileType.MOUNTAIN) || tt.equals(TileType.WATER)) return true;
            counter++;
        }
        if (counter >= 20) {
            return true;
        }
        return false;
    }

    TileType worstObstruction(Location loc) {
        Location currentLoc = uc.getLocation();
        TileType currentObstruction = null;
        int counter = 0;
        while (currentLoc.distanceSquared(loc) > 0 && counter < 20) {
            currentLoc = currentLoc.add(currentLoc.directionTo(loc));
            TileType tt = uc.senseTileTypeAtLocation(currentLoc);
            if (tt == TileType.MOUNTAIN) return tt;
            else if (tt == TileType.WATER) currentObstruction = tt;
            counter++;
        }
        return currentObstruction;
    }

    Location explore(boolean reset) {
        Location myLoc = uc.getLocation();
        findMapEdges();
        if (edgeTarget != null && myLoc.distanceSquared(edgeTarget) <= 8 || counter > 50 || reset) {
            edgeTarget = null;
            counter = 0;
        } else counter++;

        if (!edgesFound) {
            if (x1found && x2found && y1found && y2found) edgesFound = true;
        } else {
            uc.writeOnSharedArray(constants.MAP_SIZE, (x2 - x1) * (y2 - y1));
        }

        while (edgeTarget == null || (enemyBase != null && edgeTarget.distanceSquared(enemyBase) <= 50)) {
            int lowerX;
            int higherX;
            int lowerY;
            int higherY;

            if (!x1found) lowerX = myLoc.x - 70;
            else lowerX = x1;
            if (!x2found) higherX = myLoc.x + 70;
            else higherX = x2;
            if (!y1found) lowerY = myLoc.y - 70;
            else lowerY = y1;
            if (!y2found) higherY = myLoc.y + 70;
            else higherY = y2;

            edgeTarget = new Location(getRandomNumber(lowerX, higherX), getRandomNumber(lowerY, higherY));
        }

        return edgeTarget;
    }

    void kiteToBase(UnitInfo[] neutrals) {
        Location myLoc = uc.getLocation();
        Location allyBase = manager.getAllyBase();

        for (UnitInfo neutral : neutrals) {
            if (uc.isObstructed(uc.getLocation(), neutral.getLocation())) continue;
            UnitType neutralType = neutral.getType();
            if (neutralType == UnitType.MAGE || neutralType == UnitType.RANGER) continue;
            if (myLoc.distanceSquared(neutral.getLocation()) < neutral.getStat(UnitStat.VISION_RANGE) - 6) {
                moveTo(allyBase, false);
                break;
            }
        }
    }

    void moveTo(Location target, boolean reckless) {
        moveTo(target, reckless, (Direction dir)->uc.canMove(dir));
    }

    Direction[] dirs = Direction.values();

    boolean rotateRight = true; //if I should rotate right or left
    Location lastObstacleFound = null; //latest obstacle I've found in my way
    int minDistToEnemy = INF; //minimum distance I've been to the enemy while going around an obstacle
    Location prevTarget = null; //previous target

    void moveTo(Location target, boolean reckless, Function<Direction, Boolean> conditions){
        Location[] dangerLocs = dangerousLocations();

        //No target? ==> bye!
        if (target == null || !uc.canMove()) return;

        //different target? ==> previous data does not help!
        if (prevTarget == null || !target.isEqual(prevTarget)) resetPathfinding();

        //If I'm at a minimum distance to the target, I'm free!
        Location myLoc = uc.getLocation();
        int d = myLoc.distanceSquared(target);
        if (d <= minDistToEnemy) resetPathfinding();

        //Update data
        prevTarget = target;
        minDistToEnemy = Math.min(d, minDistToEnemy);

        //If there's an obstacle I try to go around it [until I'm free] instead of going to the target directly
        Direction dir = myLoc.directionTo(target);
        if (lastObstacleFound != null) dir = myLoc.directionTo(lastObstacleFound);

        //This should not happen for a single unit, but whatever
        if (conditions.apply(dir)) resetPathfinding();

        //I rotate clockwise or counterclockwise (depends on 'rotateRight'). If I try to go out of the map I change the orientation
        //Note that we have to try at most 16 times since we can switch orientation in the middle of the loop. (It can be done more efficiently)
        for (int i = 0; i < 16; ++i){
            if (conditions.apply(dir) && safeLocation(myLoc.add(dir), dangerLocs, reckless)){
                uc.move(dir);
                return;
            }
            Location newLoc = myLoc.add(dir);
            if (uc.isOutOfMap(newLoc)) rotateRight = !rotateRight;
                //If I could not go in that direction and it was not outside of the map, then this is the latest obstacle found
            else lastObstacleFound = myLoc.add(dir);
            if (rotateRight) dir = dir.rotateRight();
            else dir = dir.rotateLeft();
        }

        if (conditions.apply(dir) && safeLocation(myLoc.add(dir), dangerLocs, reckless)) uc.move(dir);
    }

    //clear some of the previous data
    private void resetPathfinding(){
        lastObstacleFound = null;
        minDistToEnemy = INF;
    }


    public int getRandomNumber(int min, int max) {
        return (int) ((uc.getRandomDouble() * (max - min)) + min);
    }

    void findMapEdges() {
        if (x1 == -1) {
            x1 = uc.readOnSharedArray(constants.MAP_EDGE_X1);
            if (x1 != -1) x1found = true;
        }
        if (x2 == 1100) {
            x2 = uc.readOnSharedArray(constants.MAP_EDGE_X2);
            if (x2 != 1100) x2found = true;
        }
        if (y1 == -1) {
            y1 = uc.readOnSharedArray(constants.MAP_EDGE_Y1);
            if (y1 != -1) y1found = true;
        }
        if (y2 == 1100) {
            y2 = uc.readOnSharedArray(constants.MAP_EDGE_Y2);
            if (y2 != 1100) y2found = true;
        }
        if (edgesFound || uc.getEnergyLeft() < 20000) return;
        if (uc.senseUnits(uc.getTeam().getOpponent()).length != 0) return;
        Location[] myLocs = uc.getVisibleLocations();
        Location myLoc = uc.getLocation();
        int range = (int)uc.getInfo().getStat(UnitStat.VISION_RANGE);
        for (Location loc: myLocs) {
            if (loc.x == myLoc.x) {
                if (!y1found && myLoc.y > loc.y && loc.y > y1 && myLoc.distanceSquared(loc.add(Direction.SOUTH)) <= range && uc.isOutOfMap(loc.add(Direction.SOUTH))) {
                    y1 = loc.y;
                    edgeTarget = null;
                    y1found = true;
                    uc.writeOnSharedArray(constants.MAP_EDGE_Y1, y1);
                }
                if (!y2found && myLoc.y < loc.y && loc.y < y2 && myLoc.distanceSquared(loc.add(Direction.NORTH)) <= range && uc.isOutOfMap(loc.add(Direction.NORTH))) {
                    y2 = loc.y;
                    edgeTarget = null;
                    y2found = true;
                    uc.writeOnSharedArray(constants.MAP_EDGE_Y2, y2);
                }
            }
            if (loc.y == myLoc.y) {
                if (!x1found && myLoc.x > loc.x && loc.x > x1 && myLoc.distanceSquared(loc.add(Direction.WEST)) <= range && uc.isOutOfMap(loc.add(Direction.WEST))) {
                    x1 = loc.x;
                    edgeTarget = null;
                    x1found = true;
                    uc.writeOnSharedArray(constants.MAP_EDGE_X1, x1);
                }
                if (!x2found && myLoc.x < loc.x && loc.x < x2 && myLoc.distanceSquared(loc.add(Direction.EAST)) <= range && uc.isOutOfMap(loc.add(Direction.EAST))) {
                    x2 = loc.x;
                    edgeTarget = null;
                    x2found = true;
                    uc.writeOnSharedArray(constants.MAP_EDGE_X2, x2);
                }
            }
        }
    }
}
