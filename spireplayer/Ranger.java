package spireplayer;

import aic2022.user.*;

public class Ranger extends MyUnit {

    RangerPathfinder pathfinder;

    Ranger(UnitController uc) {
        super(uc);

        this.pathfinder = new RangerPathfinder(uc);
    }

    void playRound() {
        genericBehaviour(true);

        attack.genericTryAttack(enemies);
        attack.genericTryAttack(neutrals);

        tryLvlUp();
        tryUseSkill(enemies, neutrals);
        tryGiveArtifacts();
        if (uc.canMove()) {
            tryMove(enemies, neutrals, false);
        }
        enemies = uc.senseUnits(opponent);
        neutrals = uc.senseUnits(Team.NEUTRAL);
        attack.genericTryAttack(enemies);
        attack.genericTryAttack(neutrals);
        attack.tryAttackShrine(uc.senseShrines());

        tryUseSkill(enemies, neutrals);
        tryGiveArtifacts();
        setOrientation();
    }

    void tryUseSkill(UnitInfo[] enemies, UnitInfo[] neutrals) {
        if (uc.getInfo().getCurrentAbilityICooldown() >= 1) return;

        for (int i = constants.ASSASSIN_LIST; i < constants.ASSASSIN_LIST + (constants.ASSASSIN_INFO_PER_CELL * constants.MAX_ASSASSIN); i += constants.ASSASSIN_INFO_PER_CELL) {
            int position = uc.readOnSharedArray(i);

            if (position != 0) {
                Location loc = helper.intToRealLocation(position);

                UnitInfo unit = uc.senseUnitAtLocation(loc);
                if (unit != null && (unit.getType() != UnitType.ASSASSIN || unit.getTeam() != myTeam.getOpponent())) {
                    manager.deleteAssassin(i);
                    continue;
                }

                if (uc.canUseFirstAbility(loc)) {
                    uc.useFirstAbility(loc);
                    uc.writeOnSharedArray(i + constants.ASSASSIN_ROOT_LEFT, uc.readOnSharedArray(i + constants.ASSASSIN_ROOT_LEFT) + 3);
                    return;
                }
            }
        }

        for (UnitInfo enemy : enemies) {
            Location enemyLoc = enemy.getLocation();
            if (enemy.getCurrentMovementCooldown() > 4) continue;
            if (uc.canUseFirstAbility(enemyLoc)) {
                uc.useFirstAbility(enemyLoc);
                return;
            }
        }

        for (UnitInfo enemy : neutrals) {
            Location enemyLoc = enemy.getLocation();
            if (enemy.getCurrentMovementCooldown() > 4) continue;
            if (uc.canUseFirstAbility(enemyLoc)) {
                uc.useFirstAbility(enemyLoc);
                return;
            }
        }
    }

    void tryLvlUp() {
        if (manager.readValue(constants.ASSASSIN_COUNTER) < 2) return;
        if (roundLastEnemySeen + 5 > round) return;
        if (uc.getInfo().getHealth() < 4/5 * uc.getInfo().getStat(UnitStat.MAX_HEALTH)) return;

        if (uc.getInfo().getLevel() == 1) {
            if (round < 1250 && uc.canLevelUp()) {
                uc.levelUp();
            }
        }
    }

    void tryMove(UnitInfo[] enemies, UnitInfo[] neutrals, boolean reckless) {
        if (uc.readOnSharedArray(constants.DESTROY_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, true, false);
        } else if (uc.readOnSharedArray(constants.GO_TO_BASE) == 1) {
            pathfinder.getNextLocationTarget(enemyBase, false, false);
        }

        Location globalShrine = manager.getClosestShrine();
        if (globalShrine != null)  {
            boolean isDangerous = manager.getShrineSafety(globalShrine) == constants.DANGEROUS_SHRINE;

            if (!isDangerous) noAttackRound = 0;
            if (uc.canSenseLocation(globalShrine)) globalShrine = null;
        }

        if (round < noAttackRound && enemies.length == 0 && neutrals.length == 0) return;

        if (closestChest != null) {
            pathfinder.getNextLocationTarget(closestChest.getLocation(), reckless, true);
            getChest();
            resetExplore = true;
        }
        else if (closestShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(closestShrine.getLocation(), reckless, false);
            resetExplore = true;
        }
        else if (globalShrine != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(globalShrine, reckless, false);
            resetExplore = true;
        }
        else if (globalEnemy != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(globalEnemy, reckless, false);
            resetExplore = true;
        }
        else if (obstructedChest != null && !mustExitDungeon) {
            pathfinder.getNextLocationTarget(obstructedChest.getLocation(), reckless, true);
            getChest();
            resetExplore = true;
        }
        else if (closestDungeon != null) {
            if (!inDungeon && canEnterDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, reckless, true);
                resetExplore = true;
            } else if (inDungeon && mustExitDungeon) {
                doDungeon();
                pathfinder.getNextLocationTarget(closestDungeon, true, true);
                resetExplore = true;
            } else {
                doDefault(reckless);
            }
        } else if (farDungeon != null && round % 400 < 50) {
            pathfinder.getNextLocationTarget(farDungeon, reckless, true);
            resetExplore = true;
        } else {
            doDefault(reckless);
        }
    }

    void doDefault(boolean reckless) {
        if (enemyBase == null) {
            pathfinder.getNextLocationTarget(move.explore(resetExplore), reckless, true);
            resetExplore = false;
        } else {
            pathfinder.getNextLocationTarget(enemyBase, reckless, true);
            resetExplore = true;
        }
    }

}
